import cv2
from enum import Enum
import matplotlib.pyplot as plt

class position(Enum):
    LEFT = 0
    MID = 1
    RIGHT = 2

class Left_ROI():
    x1 = 80
    y1 = 115
    x2 = 369
    y2 = 320

class Mid_ROI():
    x1 = 641
    y1 = 107
    x2 = 976
    y2 = 387


def drawCorrectRects(inpMat):
    # Blue color in BGR

    inpMat = cv2.cvtColor(inpMat, cv2.COLOR_HSV2BGR)

    color = (255, 0, 0)
  
    # Line thickness of 2 px
    thickness = 2


    startPoint = (Left_ROI.x1, Left_ROI.y1)
    endPoint = (Left_ROI.x2, Left_ROI.y2) 

    cv2.rectangle(inpMat, startPoint, endPoint, color, thickness)

    startPoint = (Mid_ROI.x1 , Mid_ROI.y1)
    endPoint = (Mid_ROI.x2 , Mid_ROI.y2) 

    cv2.rectangle(inpMat, startPoint, endPoint, color, thickness)

    return cv2.cvtColor(inpMat, cv2.COLOR_BGR2HSV)
    
def showImg(inpMat):
    inpMat = cv2.cvtColor(inpMat, cv2.COLOR_HSV2RGB)
    plt.imshow(inpMat)
    plt.show()

input = cv2.imread("./testImages/partYellowLeft.jpg")
mat = cv2.cvtColor(input, cv2.COLOR_BGR2HSV)

showMat =cv2.cvtColor(mat, cv2.COLOR_HSV2RGB)

plt.imshow(showMat)
plt.show()

lowerYellow = (20, 80, 130)
upperYellow = (60, 255, 255)

# newLowerYellow = (30, 127, 178)
# newUpperYellow = (70, 255, 255)

showImg(drawCorrectRects(mat))

mat = cv2.inRange(mat, lowerYellow, upperYellow)

# mat = cv2.inRange(mat, newLowerYellow, newUpperYellow)

# plt.imshow(newMat)
# plt.show()

plt.imshow(mat)
plt.show()

submatLeft = mat[Left_ROI.y1:Left_ROI.y2, Left_ROI.x1:Left_ROI.x2]
submatMid = mat[Mid_ROI.y1:Mid_ROI.y2, Mid_ROI.x1:Mid_ROI.x2]

# cv2.cvtColor(submatLeft)

plt.imshow(submatLeft)
plt.show()
plt.imshow(submatMid)
plt.show()

def rectArea(x1, x2, y1, y2):
    return (x2 - x1) * (y2 - y1)

leftValue = cv2.sumElems(submatLeft)[0] / rectArea(Left_ROI.x1, Left_ROI.x2, Left_ROI.y1, Left_ROI.y2) / 255
midValue = cv2.sumElems(submatMid)[0] / rectArea(Mid_ROI.x1, Mid_ROI.x2, Mid_ROI.y1, Mid_ROI.y2) / 255 #* (1/0.6316098081023455)


print("leftValue: " + str(leftValue))
print("midValue: " + str(midValue))

# showImg(drawCorrectRects(mat))