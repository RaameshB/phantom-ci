package org.firstinspires.ftc.teamcode.util;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.teamcode.util.RobotControlClasses.Deprecated.ChassisController2;
import org.firstinspires.ftc.teamcode.util.RobotControlClasses.CvWebcamController;
import org.firstinspires.ftc.teamcode.util.RobotControlClasses.Deprecated.ImuController;

public class RobotControl {

    LinearOpMode linearOpMode;
    RobotConfig robot;
    public ChassisController2 driveUtils;
    public ImuController imuUtils;
    public CvWebcamController cvWebcamUtils;
//    public SliderController sliderUtils;

    public RobotControl(RobotConfig hardware, LinearOpMode linearOpMode) {
        robot = hardware;
        this.linearOpMode = linearOpMode;
        driveUtils = new ChassisController2(robot, this.linearOpMode, imuUtils);
        imuUtils = new ImuController(robot, this.linearOpMode);
        cvWebcamUtils = new CvWebcamController(robot);
//        sliderUtils = new SliderController(robot, this.linearOpMode);
    }

}
