from cmath import pi, sin


def miscFuncFunction (x):
    if (x < 0):
        backwards = True
    else:
        backwards = False
    
    if (x <= 1/3):
        x *= 1.5
    else:
        x-= 1/3
        x /= 2/3
        x *= 0.5
        x += 0.5
    
    x *= 2 * pi

    sineValue = 0.5 * (sin ( x - ( pi / 2 ) ) + 1)

    if (backwards):
        return -sineValue
    else:
        return sineValue

i = 0

while (i < 1):
    print(miscFuncFunction(i))
    i += 0.1 