package org.firstinspires.ftc.teamcode.util.visionDev.ai;

import android.content.Context;
import android.graphics.Bitmap;

import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.ml.Yolov5sFp16;
import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.openftc.easyopencv.OpenCvPipeline;
import org.tensorflow.lite.DataType;
import org.tensorflow.lite.support.image.TensorImage;
import org.tensorflow.lite.support.tensorbuffer.TensorBuffer;

import java.io.IOException;

// TODO: Make this less scuffed, like why the heck does this work
public class AiPipeline extends OpenCvPipeline {

    Context context;
    Yolov5sFp16 aiModel;

    double procTime;
    ElapsedTime stopwatch = new ElapsedTime();

    public AiPipeline(Context context) {
        try {
            aiModel = Yolov5sFp16.newInstance(context);
        } catch (IOException e) {

        }

    }

    TensorImage tensorImage;

    Bitmap bitmap;

    TensorBuffer outputs;

    Object out;

    @Override
    public Mat processFrame(Mat input) {

        stopwatch.reset();

        Mat resizedMat = input;
        Imgproc.resize(input, resizedMat, new Size(640, 640));
        Imgproc.cvtColor(resizedMat, resizedMat, Imgproc.COLOR_BGR2RGB);

        try {
            bitmap = Bitmap.createBitmap(resizedMat.cols(), resizedMat.rows(), Bitmap.Config.ARGB_8888);
            Utils.matToBitmap(resizedMat, bitmap);
            tensorImage = new TensorImage(DataType.FLOAT32);
            tensorImage.load(bitmap);
            outputs = aiModel.process(tensorImage.getTensorBuffer()).getOutputFeature0AsTensorBuffer();
        } catch (Exception e) {

        }

        procTime = stopwatch.seconds();

        return input;
    }


    public double getProcTime() {
        return procTime;
    }


    public TensorBuffer getOutputs() {
        return outputs;
    }
}
