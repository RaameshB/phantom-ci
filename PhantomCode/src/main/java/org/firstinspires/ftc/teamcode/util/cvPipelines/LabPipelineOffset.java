package org.firstinspires.ftc.teamcode.util.cvPipelines;

import org.firstinspires.ftc.teamcode.util.Math.MiscFuncs;
import org.firstinspires.ftc.teamcode.util.enums.LeftAndRightEnum;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.openftc.easyopencv.OpenCvPipeline;

public class LabPipelineOffset extends OpenCvPipeline {

    static final double THRESHOLD = 133;

    static class Left_ROI {
        public static volatile int x1 = 0;
        public static volatile int y1 = 42;
        public static volatile int x2 = 122;
        public static volatile int y2 = 153;
        public static volatile Point startPoint = new Point(x1, y1);
        public static volatile Point endPoint = new Point(x2, y2);
    }
    //    LabPipeline.Left_ROI leftRoi = new LabPipeline.Left_ROI();
    static class RIGHT_ROI {
        public static volatile int x1 = 138;
        public static volatile int y1 = 42;
        public static volatile int x2 = 262;
        public static volatile int y2 = 153;
        public static volatile Point startPoint = new Point(x1, y1);
        public static volatile Point endPoint = new Point(x2, y2);
    }
//    LabPipeline.RIGHT_ROI rightRoi = new LabPipeline.RIGHT_ROI();

    public volatile boolean isBeingModified = false;

    volatile double leftValue = 0;
    volatile double rightValue = 0;

    double aTributeToTheCompletelyArbitraryValueThatMayFixThingsAlsoKnownAsAnArbitraryValueThatWeDidNotUnderstandThatFixedOurPipelineForEasyOpenCvSimulator = 1.015628337;

    volatile LeftAndRightEnum pos = LeftAndRightEnum.STILL_PROC;

    Scalar colorBlue = new Scalar(0, 0, 255);
//    Scalar colorRed = new Scalar(255, 0 , 0);
//    Scalar colorGreen = new Scalar(0, 255, 0);

    @Override
    public Mat processFrame(Mat input) {

        Mat leftBGR = input.submat(Left_ROI.y1, Left_ROI.y2, Left_ROI.x1, Left_ROI.x2);
        Mat rightBGR = input.submat(RIGHT_ROI.y1, RIGHT_ROI.y2, RIGHT_ROI.x1, RIGHT_ROI.x2);

        Mat rightSubmat = new Mat();
        Mat leftSubmat = new Mat();

        Imgproc.cvtColor(leftBGR, leftSubmat, Imgproc.COLOR_RGB2Lab);
        Imgproc.cvtColor(rightBGR, rightSubmat, Imgproc.COLOR_RGB2Lab);

        Mat aForLeft = new Mat();
        Core.extractChannel(leftSubmat, aForLeft, 1);
        Mat bForLeft = new Mat();
        Core.extractChannel(leftSubmat, bForLeft, 2);
        leftSubmat.release();
        leftValue = MiscFuncs.average(Core.mean(aForLeft).val[0], Core.mean(bForLeft).val[0]) * aTributeToTheCompletelyArbitraryValueThatMayFixThingsAlsoKnownAsAnArbitraryValueThatWeDidNotUnderstandThatFixedOurPipelineForEasyOpenCvSimulator;
        aForLeft.release();
        bForLeft.release();

        Mat aForRight = new Mat();
        Core.extractChannel(rightSubmat, aForRight, 1);
        Mat bForRight = new Mat();
        Core.extractChannel(rightSubmat, bForRight, 2);
        rightSubmat.release();
        rightValue = MiscFuncs.average(Core.mean(aForRight).val[0], Core.mean(bForRight).val[0]);
        aForRight.release();
        bForRight.release();

        if (rightValue > THRESHOLD) {
            pos = LeftAndRightEnum.RIGHT;
        } else if (leftValue > THRESHOLD) {
            pos = LeftAndRightEnum.LEFT;
        } else {
            pos = LeftAndRightEnum.NOT;
        }

        Mat rectMat = input;
        if (pos == LeftAndRightEnum.LEFT) {
            Imgproc.rectangle(rectMat, Left_ROI.startPoint, Left_ROI.endPoint, colorBlue, 2);
        } else if (pos == LeftAndRightEnum.RIGHT) {
            Imgproc.rectangle(rectMat, RIGHT_ROI.startPoint, RIGHT_ROI.endPoint, colorBlue, 2);
        }
//
//        Mat labMat = new Mat();
//        Mat aMat = new Mat();
//        Mat bMat = new Mat();
//
//        Imgproc.cvtColor(input, labMat, Imgproc.COLOR_RGB2Lab);
//        Core.extractChannel(labMat, aMat, 1);
//        Core.extractChannel(labMat, bMat, 2);
//        labMat.release();

        return rectMat;
//        return bMat;
    }

    public double getRightValue() {
        return rightValue;
    }
    public double getLeftValue() {
        return leftValue;
    }
    public LeftAndRightEnum getPos() {
        return pos;
    }
}
