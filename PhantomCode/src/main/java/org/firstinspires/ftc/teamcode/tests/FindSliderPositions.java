package org.firstinspires.ftc.teamcode.tests;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.util.RobotConfig;

import java.util.ArrayList;

//@Autonomous
@Disabled
@TeleOp
public class FindSliderPositions extends LinearOpMode {

    RobotConfig robot = new RobotConfig();

    @Override
    public void runOpMode() throws InterruptedException {
        robot.init(hardwareMap, this);
        waitForStart();
        while (!isStopRequested()) {
            telemetry.addData("Slider Motor Position: ", robot.slider.getCurrentPosition());
            telemetry.update();
            idle();
        }
    }
}
