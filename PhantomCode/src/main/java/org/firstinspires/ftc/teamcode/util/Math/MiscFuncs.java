package org.firstinspires.ftc.teamcode.util.Math;

public class MiscFuncs {
    public static double easingSine(double x) {

        if (x > 1) {
            x = 1;
        } else if (x < -1) {
            x = -1;
        }

        boolean backwards;

        if (x < 0) {
            backwards = true;
        } else {
            backwards = false;
        }

        x *= 2 * Math.PI;
        double sineValue = 0.5 * ( Math.sin ( x - ( Math.PI / 2 ) ) + 1);

        if (backwards) {
            return -sineValue;
        } else {
            return sineValue;
        }
    }

    public static double oneThirdTwoThirdEasingSine(double x) {

        if (x > 1) {
            x = 1;
        } else if (x < -1) {
            x = -1;
        }

        boolean backwards;

        if (x < 0) {
            backwards = true;
        } else {
            backwards = false;
        }

        x = Math.abs(x);

        if (x <= 1.0/3.0) {
            x *= 1.5;
        } else {
            x -= (1.0/3.0);
            x /= (2.0/3.0);
            x *= 0.5;
            x += 0.5;
        }

        x *= 2 * Math.PI;

        double sineValue = 0.5 * ( Math.sin ( x - ( Math.PI / 2 ) ) + 1);

        if (backwards) {
            return -sineValue;
        } else {
            return sineValue;
        }

    }

    public static double oneThirdTwoThirdEasingSineButItsFormattedForTheControlAward(double x) {
        if (x > 1) { x = 1; }
        else if (x < -1) { x = -1; }
        boolean backwards;
        if (x < 0) { backwards = true; }
        else { backwards = false; }
        x = Math.abs(x);
        if (x <= 1.0/3.0) { x *= 1.5; }
        else { x -= (1.0/3.0); x /= (2.0/3.0); x *= 0.5; x += 0.5; }
        x *= 2 * Math.PI;
        double sineValue = 0.5 * ( Math.sin ( x - ( Math.PI / 2 ) ) + 1);
        if (backwards) { return -sineValue; }
        else { return sineValue; }
    }


    public static double oneThirdAccelSine(double x) {

        if (x > 1) {
            x = 1;
        } else if (x < -1) {
            x = -1;
        }

        boolean backwards;

        if (x < 0) {
            backwards = true;
        } else {
            backwards = false;
        }

        x = Math.abs(x);

        double sineValue;
        if (x <= 1.0/18.0) {
            x *= 9;
            x *= 2 * Math.PI;
            sineValue = 0.5 * ( Math.sin ( x - ( Math.PI / 2 ) ) + 1);
        } else {
            sineValue = 1;
        }

        if (backwards) {
            return -sineValue;
        } else {
            return sineValue;
        }

    }

    public static double oneEighteenthAccelSineButItsFormattedForTheControlAward(double x) {
        if (x > 1) { x = 1; }
        else if (x < -1) { x = -1; }
        boolean backwards;
        if (x < 0) { backwards = true; }
        else { backwards = false; }
        x = Math.abs(x);
        double sineValue;
        if (x <= 1.0/18.0) { x *= 9; x *= 2 * Math.PI; sineValue = 0.5 * ( Math.sin ( x - ( Math.PI / 2 ) ) + 1); }
        else { sineValue = 1; }
        if (backwards) { return -sineValue; }
        else { return sineValue; }
    }

    public static double scuffedHalfEasingSine (double x) {
        if (x > 1) {
            x = 1;
        } else if (x < -1) {
            x = -1;
        }
        return easingSine(x * 0.5);
    }

    public static double scuffedHalfEasingSineButItsBackwards (double x) {
        if (x > 1) {
            x = 1;
        } else if (x < -1) {
            x = -1;
        }
        x = 1 - x;
        return easingSine(x * 0.5);
    }

    public enum easingFuncs {
        easingSine,
        oneThirdTwoThirdEasingSine,
        oneThirdAccelSine,
        scuffedHalfEasingSine,
        scuffedHalfEasingSineButItsBackwards
    }

    public static double average(double x1, double x2) {
        return (x1 + x2) / 2;
    }

    public static double easingFuncs(double x, easingFuncs easingFunc) {
        if (easingFunc == easingFuncs.easingSine) {
            return easingSine(x);
        }
        if (easingFunc == easingFuncs.oneThirdTwoThirdEasingSine) {
            return oneThirdTwoThirdEasingSine(x);
        }
        if (easingFunc == easingFuncs.oneThirdAccelSine) {
            return oneThirdAccelSine(x);
        }
        if (easingFunc == easingFuncs.scuffedHalfEasingSine) {
            return scuffedHalfEasingSine(x);
        }
        if (easingFunc == easingFuncs.scuffedHalfEasingSineButItsBackwards) {
            return scuffedHalfEasingSineButItsBackwards(x);
        }
        throw new IllegalArgumentException("didn't use a valid easingFunc");
    }
}
