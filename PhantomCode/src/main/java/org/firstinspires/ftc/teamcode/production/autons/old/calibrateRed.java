package org.firstinspires.ftc.teamcode.production.autons.old;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.util.RobotConfig;
import org.firstinspires.ftc.teamcode.util.RobotControlClasses.CvWebcamController2Red;

@Disabled
@TeleOp
public class calibrateRed extends LinearOpMode {

    RobotConfig robot = new RobotConfig();
    CvWebcamController2Red cvWebcam;

    @Override
    public void runOpMode() throws InterruptedException {
        robot.webcamOnly(hardwareMap, this);
        cvWebcam = new CvWebcamController2Red(robot);
        cvWebcam.startAsyncStream();
        while (!isStopRequested() && !isStarted()) {
            telemetry.addData("Postion: ", cvWebcam.getPos());
            telemetry.addData("mid: ", cvWebcam.getMidValue());
            telemetry.addData("right: ", cvWebcam.getRightValue());
            telemetry.addData("originalMid: ", cvWebcam.getOriginalMidValue());
            telemetry.addData("zero amount: ", cvWebcam.getZeroAmount());
            telemetry.addData("right minus mid: ", cvWebcam.getRightMinusMid());
            telemetry.update();
            idle();
        }
        waitForStart();
        cvWebcam.calibrate();
        while (!isStopRequested()) {
            telemetry.addData("Postion: ", cvWebcam.getPos());
            telemetry.addData("mid: ", cvWebcam.getMidValue());
            telemetry.addData("right: ", cvWebcam.getRightValue());
            telemetry.addData("originalMid: ", cvWebcam.getOriginalMidValue());
            telemetry.addData("zero amount: ", cvWebcam.getZeroAmount());
            telemetry.addData("right minus mid: ", cvWebcam.getRightMinusMid());
            telemetry.update();
            idle();
        }
    }
}
