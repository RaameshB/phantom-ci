package org.firstinspires.ftc.teamcode.production.autons.red;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.util.RobotConfig;
import org.firstinspires.ftc.teamcode.util.RobotControlClasses.CVC3;
import org.firstinspires.ftc.teamcode.util.RobotControlClasses.ChassisController3;
import org.firstinspires.ftc.teamcode.util.RobotControlClasses.ImuController3;
import org.firstinspires.ftc.teamcode.util.enums.LeftAndRightEnum;

@Autonomous(group = "redForComp", preselectTeleOp = "RedTeleOp")
public class RedCarouselDepot extends LinearOpMode {

    volatile RobotConfig robot;
    ImuController3 imuController;
    ChassisController3 chassis;
    CVC3 cvWebcam;

    volatile LeftAndRightEnum pos;

    boolean bot = false;

    volatile boolean goDown = false;

    Thread sliderThread = new Thread(new SliderThread());

    @Override
    public void runOpMode() throws InterruptedException {
        robot = new RobotConfig();

        robot.initButItWasBetter(hardwareMap, this);

        imuController = new ImuController3(robot, this);
        chassis = new ChassisController3(robot, this, imuController);
        cvWebcam = new CVC3(robot, this);
        cvWebcam.startAsyncStream();

        telemetry.addLine("Opening Camera...");
        telemetry.update();

        imuController.resetRelativeAngle();

        robot.intakeRotationServo.setPosition(0.7);

        while (cvWebcam.getPos() == LeftAndRightEnum.STILL_PROC && !isStopRequested()) {
            idle();
        }
        sleep(100);
        while (!isStarted() && !isStopRequested()) {
            telemetry.addLine("Waiting For Start...");
            telemetry.addData("Pos: ", cvWebcam.getPos());
            telemetry.update();
            idle();
        }
        waitForStart();
        pos = cvWebcam.getPos();
        cvWebcam.stopStream();
        sliderThread.start();
        chassis.encoderDrive(5, 2);
        chassis.pointUsingImuGlobalAngle(34.0, ChassisController3.Direction.clockwise, 2.5);
        chassis.encoderDrive(21.0, 4);
        robot.bucket.setPosition(1.0);
        sleep(1000);
        goDown = true;
        chassis.encoderDrive(-10, 3);
        chassis.pointUsingImuGlobalAngle(90, ChassisController3.Direction.clockwise, 2.5);
        chassis.encoderDrive(-20, 3);
        chassis.imuTurnFromLastSeason(-60, 2.0);
        chassis.encoderDrive(-16.25, 3);
        robot.carousel.setPower(-0.60);
        sleep(2750);
        robot.carousel.setPower(0);
        chassis.encoderDrive(5, 2);
        chassis.pointUsingImuGlobalAngle(325, ChassisController3.Direction.counterclockwise, 3);
        chassis.encoderDrive(24.5, 2);
    }

    class SliderThread implements Runnable {
        @Override
        public void run() {
            switch (pos) {
                case NOT:
                    robot.slider.setTargetPosition(-950);
                    bot = true;
                    break;
                case  LEFT:
                    robot.slider.setTargetPosition(-2400);
                    break;
                case RIGHT:
                    robot.slider.setTargetPosition(-3900);
            }
            robot.slider.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            robot.slider.setPower(1.0);
            while (robot.slider.getCurrentPosition() > -400) {

            }
            robot.bucket.setPosition(0.3);
            while (!goDown) {

            }
            if (robot.slider.getCurrentPosition() > -1500) {
                robot.bucket.setPosition(0);
                ElapsedTime runtimeThread = new ElapsedTime();
                runtimeThread.reset();
                while(runtimeThread.seconds() < 1.5) {

                }
            }
            robot.slider.setPower(0.75);
            robot.slider.setTargetPosition(0);
            while (robot.slider.getCurrentPosition() < -1500) {

            }
            robot.bucket.setPosition(0);
        }
    }

}
