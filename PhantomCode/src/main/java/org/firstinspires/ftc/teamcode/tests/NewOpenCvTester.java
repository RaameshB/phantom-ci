package org.firstinspires.ftc.teamcode.tests;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.checkerframework.checker.units.qual.C;
import org.firstinspires.ftc.teamcode.util.RobotConfig;
import org.firstinspires.ftc.teamcode.util.RobotControlClasses.CvWebcamController;
import org.firstinspires.ftc.teamcode.util.enums.Positions;

@Disabled
@TeleOp
public class NewOpenCvTester extends LinearOpMode {

    RobotConfig robot = new RobotConfig();
    double diff;

    @Override
    public void runOpMode() throws InterruptedException {
        ElapsedTime runtime = new ElapsedTime();
        ElapsedTime runtime2 = new ElapsedTime();
        robot.init(hardwareMap, this);
        CvWebcamController cvWebcamController = new CvWebcamController(robot);
        runtime.reset();
        cvWebcamController.startAsyncStream();
//        cvWebcamController.openSyncCam();
        while (cvWebcamController.getPos() == Positions.stillProc) {
            idle();
        }
        runtime2.reset();
        diff = runtime.seconds();
        waitForStart();
        while (!isStopRequested() && opModeIsActive() && runtime.seconds() < 7.5) {
/*            cvWebcamController.startSyncStream();
            sleep(2000);
            cvWebcamController.stopSyncStream();*/
            telemetry.addData("position: ", cvWebcamController.getPos());
            telemetry.addData("left: ", cvWebcamController.getLeftValue());
            telemetry.addData("mid: ", cvWebcamController.getMidValue());
            telemetry.addData("runtime: ", runtime.seconds());
            telemetry.addData("runtime2: ", runtime2.seconds());
            telemetry.addData("diff: ", diff);
            telemetry.update();
            sleep(50);
//            idle();
        }
        cvWebcamController.stopStream();
        while (!isStopRequested()) {
            idle();
        }
    }
}
