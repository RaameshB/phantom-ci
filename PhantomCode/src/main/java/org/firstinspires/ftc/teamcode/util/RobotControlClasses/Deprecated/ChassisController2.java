package org.firstinspires.ftc.teamcode.util.RobotControlClasses.Deprecated;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.navigation.Orientation;
import org.firstinspires.ftc.teamcode.util.Math.MiscFuncs;
import org.firstinspires.ftc.teamcode.util.RobotConfig;

@Deprecated
public class ChassisController2 {

    private static final double MIN_DRIVE_POWER = 0.1;
    private static final boolean IS_DIRECTION_REVERSED = false;

    private double getAbsRelativeAngle() {
        return Math.abs(imuController.getRelativeAngle());
    }


    public void imuTurn(double angle, Direction direction, boolean useGlobalAngle) {
        hardware.rightDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        hardware.leftDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        double deltaAngle;

        if (useGlobalAngle) {
            if (direction == Direction.clockwise && angle < imuController.getGlobalAngle()) {
                deltaAngle = (360 - imuController.getGlobalAngle()) + angle;
            } else if (direction == Direction.counterclockwise && angle > imuController.getGlobalAngle()) {
                deltaAngle = -((360 - angle) + imuController.getGlobalAngle());
            } else {
                deltaAngle = angle - imuController.getGlobalAngle();
            }
        } else {
            deltaAngle = angle;
        }

        imuController.resetRelativeAngle();

        double maxPower = Math.abs(deltaAngle / 60);

        if (maxPower > 1) {
            maxPower = 1;
        }

        double powerPercent;
        double percentCompleteAbs = imuController.getRelativeAngle() / deltaAngle;
        double percentCompleteVectorThing;
        double absDeltaAngle = Math.abs(deltaAngle);
        double relativeAngle;

        while (percentCompleteAbs < 100 || Math.toDegrees(Math.abs((hardware.imu.getAngularVelocity().zRotationRate))) < 5) {
            relativeAngle = imuController.getRelativeAngle();
            percentCompleteAbs = relativeAngle / deltaAngle;

            if ( (deltaAngle < 0 && relativeAngle < deltaAngle) || (deltaAngle > 0 && relativeAngle > deltaAngle) ) {
                percentCompleteVectorThing = (deltaAngle - relativeAngle) / absDeltaAngle;
                ln.telemetry.addLine("0:0");
            } else {
                percentCompleteVectorThing = relativeAngle / absDeltaAngle;
                ln.telemetry.addLine("0:1");
            }

            if (percentCompleteVectorThing < 0) {
                powerPercent = Math.max(0.1, MiscFuncs.oneThirdTwoThirdEasingSine(percentCompleteVectorThing) * maxPower);
                ln.telemetry.addLine("1:0");
            } else {
                powerPercent = Math.min(-0.1, MiscFuncs.oneThirdTwoThirdEasingSine(percentCompleteVectorThing) * maxPower);
                ln.telemetry.addLine("1:1");
            }

            rotate(powerPercent);

            ln.telemetry.addData("angle", relativeAngle);
            ln.telemetry.addData("power: ", powerPercent);
            ln.telemetry.addData("percentComplete: ", percentCompleteAbs);
            ln.telemetry.addData("AngVeloc: ", hardware.imu.getAngularVelocity().zRotationRate);
            ln.telemetry.update();
        }
    }



    public void imuTurn(double maxPower, double angle, Direction direction, boolean useGlobalAngle) {
        hardware.rightDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        hardware.leftDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        double deltaAngle = 0;

        if (useGlobalAngle) {
            if (direction == Direction.clockwise && angle < imuController.getGlobalAngle()) {
                deltaAngle = (360 - imuController.getGlobalAngle()) + angle;
            } else if (direction == Direction.counterclockwise && angle > imuController.getGlobalAngle()) {
                deltaAngle = -((360 - angle) + imuController.getGlobalAngle());
            } else {
                deltaAngle = angle - imuController.getGlobalAngle();
            }
        } else {
            deltaAngle = angle;
        }

        imuController.resetRelativeAngle();

        double powerPercent;


        double percentCompleteAbs = imuController.getRelativeAngle() / deltaAngle;
        double percentCompleteVectorThing;
        double absDeltaAngle = Math.abs(deltaAngle);

        while (percentCompleteAbs < 100 || Math.toDegrees(Math.abs((hardware.imu.getAngularVelocity().zRotationRate))) < 5) {
            percentCompleteAbs = imuController.getRelativeAngle() / deltaAngle;

            if ( (deltaAngle < 0 && imuController.getRelativeAngle() < deltaAngle) || (deltaAngle > 0 && imuController.getRelativeAngle() > deltaAngle) ) {
                percentCompleteVectorThing = (deltaAngle - imuController.getRelativeAngle()) / absDeltaAngle;
            } else {
                percentCompleteVectorThing = imuController.getRelativeAngle() / absDeltaAngle;
            }

            if (percentCompleteVectorThing < 0) {
                powerPercent = Math.min(-0.1, MiscFuncs.oneThirdTwoThirdEasingSine(percentCompleteVectorThing) * maxPower);
            } else {
                powerPercent = Math.max(0.1, MiscFuncs.oneThirdTwoThirdEasingSine(percentCompleteVectorThing) * maxPower);
            }

            rotate(powerPercent);

            ln.telemetry.addData("angle", imuController.getRelativeAngle());
            ln.telemetry.addData("power: ", powerPercent);
            ln.telemetry.addData("percentComplete: ", percentCompleteAbs);
            ln.telemetry.addData("AngVeloc: ", hardware.imu.getAngularVelocity().zRotationRate);
            ln.telemetry.update();
        }
    }

    static final double COUNTS_PER_REVOLUTION = 1120;
    static final double SPROCKET_REDUCTION = 1.5;
    static final double GEAR_REDUCTION = 0.5;
    static final double WHEEL_DIAMETER_INCHES = 4;
    static final double COUNTS_PER_INCH = (COUNTS_PER_REVOLUTION * SPROCKET_REDUCTION * GEAR_REDUCTION) / (WHEEL_DIAMETER_INCHES * Math.PI);

    public void encoderDrive(double maxPower, double distance, double timeoutS) {
        int newLeftTarget;
        int newRightTarget;

        int originalLeft;
        int originalRight;

        ElapsedTime runtime = new ElapsedTime();

        // Ensure that the opmode is still active
        if (ln.opModeIsActive()) {

            // Determine new target position, and pass to motor controller

            int distToCounts = (int)(distance * COUNTS_PER_INCH);

            originalLeft = hardware.leftDrive.motor1.getCurrentPosition();
            originalRight = hardware.rightDrive.motor1.getCurrentPosition();
            newLeftTarget = hardware.leftDrive.motor1.getCurrentPosition() + distToCounts;
            newRightTarget = hardware.rightDrive.motor1.getCurrentPosition() + distToCounts;
            hardware.leftDrive.setTargetPosition(newLeftTarget);
            hardware.rightDrive.setTargetPosition(newRightTarget);


            // Turn On RUN_TO_POSITION
            hardware.leftDrive.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            hardware.rightDrive.setMode(DcMotor.RunMode.RUN_TO_POSITION);

            // reset the timeout time and start motion.
            runtime.reset();

            // keep looping while we are still active, and there is time left, and both motors are running.
            // Note: We use (isBusy() && isBusy()) in the loop test, which means that when EITHER motor hits
            // its target position, the motion will stop.  This is "safer" in the event that the robot will
            // always end the motion as soon as possible.
            // However, if you require that BOTH motors have finished their moves before the robot continues
            // onto the next step, use (isBusy() || isBusy()) in the loop test.

            double percentComplete;
            double speed;
            while (ln.opModeIsActive() &&
                    (runtime.seconds() < timeoutS) &&
                    (hardware.leftDrive.motor1.isBusy() && hardware.rightDrive.motor1.isBusy())) {

                percentComplete = (((hardware.leftDrive.motor1.getCurrentPosition() - originalLeft)/ (double) distToCounts) + ((hardware.rightDrive.motor1.getCurrentPosition() - originalRight)/ (double) distToCounts))/2;
                speed = Math.max(0.1,Math.abs(MiscFuncs.oneThirdTwoThirdEasingSine(percentComplete) * maxPower));
                hardware.leftDrive.setPower(speed);
                hardware.rightDrive.setPower(speed);
                // Display it for the driver.
                ln.telemetry.addData("Path1",  "Running to %7d :%7d", newLeftTarget,  newRightTarget);
                ln.telemetry.addData("Path2",  "Running at %7d :%7d",
                        hardware.leftDrive.motor1.getCurrentPosition(),
                        hardware.rightDrive.motor1.getCurrentPosition());
                ln.telemetry.update();
            }

            // Stop all motion;
            hardware.leftDrive.setPower(0);
            hardware.rightDrive.setPower(0);

            hardware.rightDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
            hardware.leftDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

            // Turn off RUN_TO_POSITION
            hardware.leftDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            hardware.rightDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

            //  sleep(250);   // optional pause after each move
        }
    }

    public void encoderDrive(double distance, double timeoutS) {
        int newLeftTarget;
        int newRightTarget;

        int originalLeft;
        int originalRight;

        ElapsedTime runtime = new ElapsedTime();

        // Ensure that the opmode is still active
        if (ln.opModeIsActive()) {

            // Determine new target position, and pass to motor controller

            int distToCounts = (int)(distance * COUNTS_PER_INCH);

            originalLeft = hardware.leftDrive.motor1.getCurrentPosition();
            originalRight = hardware.rightDrive.motor1.getCurrentPosition();
            newLeftTarget = hardware.leftDrive.motor1.getCurrentPosition() + distToCounts;
            newRightTarget = hardware.rightDrive.motor1.getCurrentPosition() + distToCounts;
            hardware.leftDrive.setTargetPosition(newLeftTarget);
            hardware.rightDrive.setTargetPosition(newRightTarget);


            // Turn On RUN_TO_POSITION
            hardware.leftDrive.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            hardware.rightDrive.setMode(DcMotor.RunMode.RUN_TO_POSITION);

            // reset the timeout time and start motion.
            runtime.reset();

            // keep looping while we are still active, and there is time left, and both motors are running.
            // Note: We use (isBusy() && isBusy()) in the loop test, which means that when EITHER motor hits
            // its target position, the motion will stop.  This is "safer" in the event that the robot will
            // always end the motion as soon as possible.
            // However, if you require that BOTH motors have finished their moves before the robot continues
            // onto the next step, use (isBusy() || isBusy()) in the loop test.
            double maxPower = Math.abs(distance) / 30;
            if (maxPower > 1) {
                maxPower = 1;
            }
            double percentComplete;
            double speed;
            while (ln.opModeIsActive() &&
                    (runtime.seconds() < timeoutS) &&
                    (hardware.leftDrive.motor1.isBusy() && hardware.rightDrive.motor1.isBusy())) {

                percentComplete = (((hardware.leftDrive.motor1.getCurrentPosition() - originalLeft)/ (double) distToCounts) + ((hardware.rightDrive.motor1.getCurrentPosition() - originalRight)/ (double) distToCounts))/2;
                speed = Math.max(0.1,Math.abs(MiscFuncs.oneThirdTwoThirdEasingSine(percentComplete) * maxPower));
                hardware.leftDrive.setPower(speed);
                hardware.rightDrive.setPower(speed);
                // Display it for the driver.
                ln.telemetry.addData("Path1",  "Running to %7d :%7d", newLeftTarget,  newRightTarget);
                ln.telemetry.addData("Path2",  "Running at %7d :%7d",
                        hardware.leftDrive.motor1.getCurrentPosition(),
                        hardware.rightDrive.motor1.getCurrentPosition());
                ln.telemetry.update();
            }

            // Stop all motion;
            hardware.leftDrive.setPower(0);
            hardware.rightDrive.setPower(0);

            hardware.rightDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
            hardware.leftDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

            // Turn off RUN_TO_POSITION
            hardware.leftDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            hardware.rightDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

            //  sleep(250);   // optional pause after each move
        }
    }

    public void encoderDriveFirstThirdAccel(double maxPower, double distance, double timeoutS) {
        int newLeftTarget;
        int newRightTarget;

        int originalLeft;
        int originalRight;

        ElapsedTime runtime = new ElapsedTime();

        // Ensure that the opmode is still active
        if (ln.opModeIsActive()) {

            // Determine new target position, and pass to motor controller

            int distToCounts = (int)(distance * COUNTS_PER_INCH);

            originalLeft = hardware.leftDrive.motor1.getCurrentPosition();
            originalRight = hardware.rightDrive.motor1.getCurrentPosition();
            newLeftTarget = hardware.leftDrive.motor1.getCurrentPosition() + distToCounts;
            newRightTarget = hardware.rightDrive.motor1.getCurrentPosition() + distToCounts;
            hardware.leftDrive.setTargetPosition(newLeftTarget);
            hardware.rightDrive.setTargetPosition(newRightTarget);


            // Turn On RUN_TO_POSITION
            hardware.leftDrive.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            hardware.rightDrive.setMode(DcMotor.RunMode.RUN_TO_POSITION);

            // reset the timeout time and start motion.
            runtime.reset();

            // keep looping while we are still active, and there is time left, and both motors are running.
            // Note: We use (isBusy() && isBusy()) in the loop test, which means that when EITHER motor hits
            // its target position, the motion will stop.  This is "safer" in the event that the robot will
            // always end the motion as soon as possible.
            // However, if you require that BOTH motors have finished their moves before the robot continues
            // onto the next step, use (isBusy() || isBusy()) in the loop test.
            double percentComplete;
            double speed;
            while (ln.opModeIsActive() &&
                    (runtime.seconds() < timeoutS) &&
                    (hardware.leftDrive.motor1.isBusy() && hardware.rightDrive.motor1.isBusy())) {

                percentComplete = (((hardware.leftDrive.motor1.getCurrentPosition() - originalLeft)/ (double) distToCounts) + ((hardware.rightDrive.motor1.getCurrentPosition() - originalRight)/ (double) distToCounts))/2;
                speed = Math.max(0.1,Math.abs(MiscFuncs.oneThirdAccelSine(percentComplete) * maxPower));
                hardware.leftDrive.setPower(speed);
                hardware.rightDrive.setPower(speed);
                // Display it for the driver.
                ln.telemetry.addData("Path1",  "Running to %7d :%7d", newLeftTarget,  newRightTarget);
                ln.telemetry.addData("Path2",  "Running at %7d :%7d",
                        hardware.leftDrive.motor1.getCurrentPosition(),
                        hardware.rightDrive.motor1.getCurrentPosition());
                ln.telemetry.update();
            }

            // Stop all motion;
            hardware.leftDrive.setPower(0);
            hardware.rightDrive.setPower(0);

            hardware.rightDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
            hardware.leftDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

            // Turn off RUN_TO_POSITION
            hardware.leftDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            hardware.rightDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

            //  sleep(250);   // optional pause after each move
        }
    }

    public enum whichSide {
        LEFT,
        RIGHT
    }

    public void moveWhileMaintainingHeading(double power, whichSide side, double timeS) {
        ElapsedTime runtime = new ElapsedTime();

        if (power > 0.5) {
            power = 0.5;
        } else if (power < 0.5) {
            power = -0.5;
        }

        double gain = 0.7;

        imuController.resetRelativeAngle();

        double correctionPower;

        if (side == whichSide.LEFT) {
            hardware.rightDrive.setPower(power);
            hardware.leftDrive.setPower(power);
            while (ln.opModeIsActive() && Math.abs(imuController.getRelativeAngle()) < 5) {
                ln.telemetry.addData("unboundRelAng: ", imuController.getRelativeAngle());
                ln.telemetry.update();
                ln.idle();
            }
            runtime.reset();
            while (ln.opModeIsActive() && runtime.seconds() < timeS) {
                correctionPower = -(imuController.getRelativeAngle()/30) * gain;
                hardware.leftDrive.setPower(correctionPower);
            }
        } else if (side == whichSide.RIGHT /* && ln.opModeIsActive() */) {
            hardware.rightDrive.setPower(power);
            hardware.leftDrive.setPower(power);
            while (ln.opModeIsActive() && Math.abs(imuController.getRelativeAngle()) < 5) {
                ln.telemetry.addData("unboundRelAng: ", imuController.getRelativeAngle());
                ln.telemetry.update();
                ln.idle();
            }
            runtime.reset();
            while (ln.opModeIsActive() && runtime.seconds() < timeS) {
                correctionPower = (imuController.getRelativeAngle()/30) * gain;
                hardware.rightDrive.setPower(correctionPower);
            }
        }
    }

    @Deprecated
    public void imuTurn(double maxPower, double angle, boolean useGlobalAngle) {
        hardware.rightDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        hardware.leftDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        double deltaAngle = 0;

        if (useGlobalAngle) {

        }

        imuController.resetRelativeAngle();

//        deltaAngle = 359;

        double powerPercent;

        Direction direction;

        if (deltaAngle > 0) {
            direction = Direction.counterclockwise;
        } else if (deltaAngle < 0) {
            direction = Direction.clockwise;
        } else {
            return;
        }

        deltaAngle = Math.abs(deltaAngle);

        double x;

        while (getAbsRelativeAngle() < deltaAngle && ln.opModeIsActive()) {
            x = getAbsRelativeAngle() / deltaAngle;
//            MiscFuncs misc = new MiscFuncs();
            powerPercent = Math.max(MiscFuncs.easingSine(x) * maxPower, 0.1);
            rotate(powerPercent, direction);
            ln.telemetry.addData("angle", imuController.getRelativeAngle());
            ln.telemetry.addData("power: ", powerPercent);
            ln.telemetry.update();
        }
    }

    @Deprecated
    public void imuTurn (double angle, EasingTypes easing, boolean useGlobalAngle) {
        hardware.rightDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        hardware.leftDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        double deltaAngle = 0;

        if (useGlobalAngle) {
            if (angle > 360) {
                do {
                    angle -= 360;
                } while (angle > 360);
            } else if (angle < 0) {
                do {
                    angle += 360;
                } while (angle < 0);
            }
            deltaAngle = angle - imuController.getGlobalAngle();

            if (deltaAngle > 180) {
                deltaAngle -= 360;
            } else if (deltaAngle < -180) {
                deltaAngle += 360;
            }

        } else if (angle > 360) {
            do {
                angle -= 360;
            } while (angle > 360);
            if (angle > 180) {
                angle = 180 - angle;
            }
        } else if (angle < -360) {
            do {
                angle += 360;
            } while (angle < -360);
            if (angle < -180) {
                angle = -180 - angle;
            }

            deltaAngle = angle;
        }



        double midway = deltaAngle / 2;

        imuController.resetRelativeAngle();

        double powerPercent;

        if (easing == EasingTypes.PROPORTIONAL) {
            if (deltaAngle > 0) {
                while (imuController.getRelativeAngle() < midway) {
                    powerPercent = imuController.getRelativeAngle() / midway;
                    rotate(Math.max(powerPercent, 0.1), ChassisController2.Direction.counterclockwise);
                    ln.telemetry.addData("angle", imuController.getRelativeAngle());
                    ln.telemetry.addData("power: ", powerPercent);
                    ln.telemetry.update();
                }
                while (imuController.getRelativeAngle() <= deltaAngle) {
                    powerPercent = 1 - ((imuController.getRelativeAngle() - midway)/midway);
                    rotate(Math.max(powerPercent, 0.1), ChassisController2.Direction.counterclockwise);
                    ln.telemetry.addData("angle", imuController.getRelativeAngle());
                    ln.telemetry.addData("power: ", powerPercent);
                    ln.telemetry.update();
                }
            }
            if (deltaAngle < 0) {
                while (imuController.getRelativeAngle() > midway) {
                    powerPercent = imuController.getRelativeAngle() / midway;
                    rotate(Math.max(powerPercent, 0.1), ChassisController2.Direction.counterclockwise);
                    ln.telemetry.addData("angle", imuController.getRelativeAngle());
                    ln.telemetry.addData("power: ", powerPercent);
                    ln.telemetry.update();
                }
                while (imuController.getRelativeAngle() >= deltaAngle) {
                    powerPercent = 1 - ((imuController.getRelativeAngle() - midway)/midway);
                    rotate(Math.max(powerPercent, 0.1), ChassisController2.Direction.clockwise);
                    ln.telemetry.addData("angle", imuController.getRelativeAngle());
                    ln.telemetry.addData("power: ", powerPercent);
                    ln.telemetry.update();
                }
            }
        }
    }

    @Deprecated
    public void imuTurn (double maxPower, double angle, EasingTypes easing, boolean useGlobalAngle) {
        hardware.rightDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        hardware.leftDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        double deltaAngle = 0;

        if (useGlobalAngle) {
            if (angle > 360) {
                do {
                    angle -= 360;
                } while (angle > 360);
            } else if (angle < 0) {
                do {
                    angle += 360;
                } while (angle < 0);
            }
            deltaAngle = angle - imuController.getGlobalAngle();

            if (deltaAngle > 180) {
                deltaAngle -= 360;
            } else if (deltaAngle < -180) {
                deltaAngle += 360;
            }

        } else if (angle > 360) {
            do {
                angle -= 360;
            } while (angle > 360);
            if (angle > 180) {
                angle = 180 - angle;
            }
        } else if (angle < -360) {
            do {
                angle += 360;
            } while (angle < -360);
            if (angle < -180) {
                angle = -180 - angle;
            }

            deltaAngle = angle;
        }

        double midway = deltaAngle / 2;

        imuController.resetRelativeAngle();

        double powerPercent;

        if (easing == EasingTypes.PROPORTIONAL) {
            if (deltaAngle < 0) {
                while (imuController.getRelativeAngle() < midway) {
                    powerPercent = imuController.getRelativeAngle() / midway;
                    rotate(Math.max(powerPercent  * maxPower, 0.2), Direction.counterclockwise);
                }
                while (imuController.getRelativeAngle() < deltaAngle) {
                    powerPercent = midway / (imuController.getRelativeAngle() - midway);
                    rotate(Math.max(powerPercent * maxPower, 0.2), Direction.counterclockwise);
                }
            }
            if (deltaAngle > 0) {
                while (imuController.getRelativeAngle() > midway) {
                    powerPercent = imuController.getRelativeAngle() / midway;
                    rotate(Math.max(powerPercent * maxPower, 0.2), Direction.clockwise);
                }
                while (imuController.getRelativeAngle() >= deltaAngle) {
                    powerPercent = midway / (imuController.getRelativeAngle() - midway);
                    rotate(Math.max(powerPercent * maxPower, 0.2), Direction.clockwise);
                }
            }
        }
        hardware.rightDrive.setPower(0);
        hardware.leftDrive.setPower(0);
    }

    void rotate(double power, ChassisController2.Direction direction) {
        if (direction == ChassisController2.Direction.clockwise) {
            hardware.leftDrive.setPower(power);
            hardware.rightDrive.setPower(-power);
        } else {
            hardware.leftDrive.setPower(-power);
            hardware.rightDrive.setPower(power);
        }
    }

    void rotate(double power) {
        hardware.leftDrive.setPower(-power);
        hardware.rightDrive.setPower(power);
    }

    RobotConfig hardware;
    LinearOpMode ln;
    Orientation angles;
    Orientation lastAngles;
    ImuController imuController;
    public ChassisController2(RobotConfig robot, LinearOpMode ln, ImuController imuController) {
        hardware = robot;
        this.ln = ln;
//        robot = robot;
        this.imuController = imuController;
//        ln = ln;
    }
    public enum EasingTypes {
        PROPORTIONAL,
        QUADRATIC,
        SINE
    }
    public enum Direction {
        clockwise,
        counterclockwise
    }
}
