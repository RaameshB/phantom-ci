# noinspection PyInterpreter
import cv2
from enum import Enum
import matplotlib.pyplot as plt



# Vars
class Left_ROI():
    x1 = 80
    y1 = 115
    x2 = 369
    y2 = 320
class Mid_ROI():
    x1 = 641
    y1 = 107
    x2 = 976
    y2 = 387
lowerYellow = (20, 80, 130)
upperYellow = (60, 255, 255)
PERCENT_COLOUR_THRESHOLD = 0.4

#Position Enum
class position(Enum):
    LEFT = 0
    MID = 1
    RIGHT = 2

#Util Functions
def drawCorrectRects(inpMat):
    # Blue color in BGR

    inpMat = cv2.cvtColor(inpMat, cv2.COLOR_HSV2BGR)

    color = (255, 0, 0)
  
    # Line thickness of 2 px
    thickness = 2


    startPoint = (Left_ROI.x1, Left_ROI.y1)
    endPoint = (Left_ROI.x2, Left_ROI.y2) 

    cv2.rectangle(inpMat, startPoint, endPoint, color, thickness)

    startPoint = (Mid_ROI.x1 , Mid_ROI.y1)
    endPoint = (Mid_ROI.x2 , Mid_ROI.y2) 

    cv2.rectangle(inpMat, startPoint, endPoint, color, thickness)

    return cv2.cvtColor(inpMat, cv2.COLOR_BGR2HSV)
def showImg(inpMat):
    inpMat = cv2.cvtColor(inpMat, cv2.COLOR_HSV2RGB)
    plt.imshow(inpMat)
    plt.show()
def rectArea(x1, x2, y1, y2):
    return (x2 - x1) * (y2 - y1)


input = cv2.imread("./testImages/partYellowRight.jpg")



mat = cv2.cvtColor(input, cv2.COLOR_BGR2HSV)

showImg(mat)

showImg(drawCorrectRects(mat))

submatLeft = mat[Left_ROI.y1:Left_ROI.y2, Left_ROI.x1:Left_ROI.x2]
submatMid = mat[Mid_ROI.y1:Mid_ROI.y2, Mid_ROI.x1:Mid_ROI.x2]

showImg(submatLeft)
showImg(submatMid)

mat = cv2.inRange(mat, lowerYellow, upperYellow)

submatLeft = mat[Left_ROI.y1:Left_ROI.y2, Left_ROI.x1:Left_ROI.x2]
submatMid = mat[Mid_ROI.y1:Mid_ROI.y2, Mid_ROI.x1:Mid_ROI.x2]

print(cv2.sumElems(submatLeft)[0])

leftValue = cv2.sumElems(submatLeft)[0] / rectArea(Left_ROI.x1, Left_ROI.x2, Left_ROI.y1, Left_ROI.y2) / 255
midValue = cv2.sumElems(submatMid)[0] / rectArea(Mid_ROI.x1, Mid_ROI.x2, Mid_ROI.y1, Mid_ROI.y2) / 255

print("leftValue: " + str(leftValue))
print("midValue: " + str(midValue))

pos = position.RIGHT

if (leftValue > PERCENT_COLOUR_THRESHOLD):
    pos = position.LEFT
elif (midValue > PERCENT_COLOUR_THRESHOLD):
    pos = position.MID

print(pos)



inpMat = input

colorBlue = (255, 0, 0)
colorRed = (0, 0 , 255)
colorGreen = (0, 255, 0)

# Line thickness of 2 px
thickness = 2


startPoint = (Left_ROI.x1, Left_ROI.y1)
endPoint = (Left_ROI.x2, Left_ROI.y2) 

startPoint2 = (Mid_ROI.x1 , Mid_ROI.y1)
endPoint2 = (Mid_ROI.x2 , Mid_ROI.y2) 

if (pos == position.LEFT):
    cv2.rectangle(inpMat, startPoint, endPoint, colorRed, thickness)
    cv2.rectangle(inpMat, startPoint2, endPoint2, colorBlue, thickness)
elif(pos == position.MID):
    cv2.rectangle(inpMat, startPoint, endPoint, colorBlue, thickness)
    cv2.rectangle(inpMat, startPoint2, endPoint2, colorRed, thickness)
elif(pos == position.RIGHT):
    cv2.rectangle(inpMat, startPoint, endPoint, colorGreen, thickness)
    cv2.rectangle(inpMat, startPoint2, endPoint2, colorGreen, thickness)

inpMat = cv2.cvtColor(inpMat, cv2.COLOR_BGR2HSV)

showImg(inpMat)