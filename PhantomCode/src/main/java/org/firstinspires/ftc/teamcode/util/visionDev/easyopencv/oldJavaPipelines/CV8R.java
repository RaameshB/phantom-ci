package org.firstinspires.ftc.teamcode.util.visionDev.easyopencv.oldJavaPipelines;

import org.firstinspires.ftc.teamcode.util.enums.Positions;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.openftc.easyopencv.OpenCvPipeline;

public class CV8R extends OpenCvPipeline {

    Positions pos;


    public double amountAboveZero = 0.015;

    public double rightMinusMid = 0;
    public double zeroAmount = 0.334;

    {
        pos = Positions.stillProc;
    }
//
//    static final double PERCENT_COLOUR_THRESHOLD_MID = 0.235;
//    static final double PERCENT_COLOUR_THRESHOLD_RIGHT = 0.3075;



    public void setRightMinusMid(double rightMinusMid) {
        this.rightMinusMid = rightMinusMid;
    }


    public void setZeroAmount(double zeroAmount) {
        this.zeroAmount = zeroAmount;
    }

    class Mid_ROI {
        public int x1 = 0;
        public int y1 = 40;
        public int x2 = 97;
        public int y2 = 78;
        public Point startPoint = new Point(x1, y1);
        public Point endPoint = new Point(x2, y2);
    }
    Mid_ROI midRoi = new Mid_ROI();
    class RIGHT_ROI {
        public int x1 = 114;
        public int y1 = 39;
        public int x2 = 223;
        public int y2 = 77;
        public Point startPoint = new Point(x1, y1);
        public Point endPoint = new Point(x2, y2);
    }
    RIGHT_ROI rightRoi = new RIGHT_ROI();

    Scalar lowerYellow = new Scalar(20, 80, 130);
    Scalar upperYellow = new Scalar(60, 255, 255);

    Scalar colorBlue = new Scalar(0, 0, 255);
    Scalar colorRed = new Scalar(255, 0 , 0);
    Scalar colorGreen = new Scalar(0, 255, 0);

    int thickness = 2;

    public int rectArea(int x1, int x2, int y1, int y2) {
        return (x2 - x1) * (y2 - y1);
    }

    double rightValue = 0;// = Core.sumElems(leftSubmat).val[0] / rectArea(leftRoi.x1, leftRoi.x2, leftRoi.y1, leftRoi.y2) / 255 * aCompletelyArbitraryValueThatMayFixThings;
    double midValue = 0;
    double originalMidValue = 0;

    @Override
    public Mat processFrame(Mat input) {
        Mat hsvImg = new Mat();
        Imgproc.cvtColor(input, hsvImg, Imgproc.COLOR_BGR2HSV);

        Mat inRangeMat = new Mat();
        Core.inRange(hsvImg, lowerYellow, upperYellow, inRangeMat);

//        Core.inRange(hsvImg, lowerYellow, upperYellow, inRangeMat);

        Mat midSubmat = hsvImg.submat(midRoi.y1, midRoi.y2, midRoi.x1, midRoi.x2);
        Mat rightSubmat = hsvImg.submat(rightRoi.y1, rightRoi.y2, rightRoi.x1, rightRoi.x2);

        rightValue = Core.sumElems(rightSubmat).val[0] / rectArea(rightRoi.x1, rightRoi.x2, rightRoi.y1, rightRoi.y2) / 255;
        originalMidValue = Core.sumElems(midSubmat).val[0] / rectArea(midRoi.x1, midRoi.x2, midRoi.y1, midRoi.y2) / 255;

        midValue = originalMidValue + rightMinusMid;

        rightSubmat.release();
        midSubmat.release();
        hsvImg.release();

        if (rightValue > zeroAmount + amountAboveZero) {
            pos = Positions.rightTop;
        } else if (midValue > zeroAmount + amountAboveZero) {
            pos = Positions.midMid;
        } else {
            pos = Positions.leftBot;
        }

        Mat rectMat = input;
        if (pos == Positions.rightTop) {
            Imgproc.rectangle(rectMat, rightRoi.startPoint, rightRoi.endPoint, colorBlue, thickness);
            Imgproc.rectangle(rectMat, midRoi.startPoint, midRoi.endPoint, colorRed, thickness);
        } else if (pos == Positions.midMid) {
            Imgproc.rectangle(rectMat, rightRoi.startPoint, rightRoi.endPoint, colorRed, thickness);
            Imgproc.rectangle(rectMat, midRoi.startPoint, midRoi.endPoint, colorBlue, thickness);
        } else {
            Imgproc.rectangle(rectMat, rightRoi.startPoint, rightRoi.endPoint, colorGreen, thickness);
            Imgproc.rectangle(rectMat, midRoi.startPoint, midRoi.endPoint, colorGreen, thickness);
        }

        return rectMat;
    }

    public Positions getPos() {
        return pos;
    }
    public double getRightValue() {
        return rightValue;
    }
    public double getMidValue() {
        return  midValue;
    }
    public double getOriginalMidValue() {
        return originalMidValue;
    }
    public double getZeroAmount() {
        return zeroAmount;
    }
    public double getRightMinusMid() {
        return rightMinusMid;
    }
}
