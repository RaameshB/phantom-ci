package org.firstinspires.ftc.teamcode.production.autons.blue;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.util.RobotConfig;
import org.firstinspires.ftc.teamcode.util.RobotControlClasses.CVC3Offset;
import org.firstinspires.ftc.teamcode.util.RobotControlClasses.ChassisController3;
import org.firstinspires.ftc.teamcode.util.RobotControlClasses.ImuController3;
import org.firstinspires.ftc.teamcode.util.enums.LeftAndRightEnum;

@Autonomous(group = "blueForComp", preselectTeleOp = "BlueTeleOp")
public class BlueCarouselDepot extends LinearOpMode {

    volatile RobotConfig robot;
    ImuController3 imuController;
    ChassisController3 chassis;
    CVC3Offset cvWebcam;

    LeftAndRightEnum pos;

    volatile boolean goDown = false;

    Thread sliderThread = new Thread(new SliderThread());

    @Override
    public void runOpMode() throws InterruptedException {
        robot = new RobotConfig();

        robot.initButItWasBetter(hardwareMap, this);

        imuController = new ImuController3(robot, this);
        chassis = new ChassisController3(robot, this, imuController);
        cvWebcam = new CVC3Offset(robot, this);
        cvWebcam.startAsyncStream();

        telemetry.addLine("Opening Camera...");
        telemetry.update();

        imuController.resetRelativeAngle();

        while (cvWebcam.getPos() == LeftAndRightEnum.STILL_PROC && !isStopRequested()) {
            idle();
        }
        sleep(500);
        while (!isStarted() && !isStopRequested()) {
            telemetry.addLine("Waiting For Start...");
            telemetry.addData("Pos: ", cvWebcam.getPos());
            telemetry.update();
            idle();
        }
        waitForStart();
        pos = cvWebcam.getPos();
        cvWebcam.stopStream();
        sliderThread.start();
        chassis.encoderDrive(5, 2);
        robot.bucket.setPosition(0.3);
        chassis.pointUsingImuGlobalAngle(326.0, ChassisController3.Direction.counterclockwise, 2.5);
        chassis.encoderDrive(22.0, 4);
        robot.bucket.setPosition(1.0);
        sleep(1000);
        goDown = true;
        chassis.encoderDrive(-10, 3);
        chassis.pointUsingImuGlobalAngle(292, ChassisController3.Direction.counterclockwise, 2.5);
        chassis.encoderDrive(-31, 4);
        robot.carousel.setPower(0.6);
        sleep(2750);
        robot.carousel.setPower(0);
        chassis.encoderDrive(5, 2);
        chassis.pointUsingImuGlobalAngle(25, ChassisController3.Direction.clockwise, 2.5);
        chassis.encoderDrive(26, 2);
//        chassis.imuTurnFromLastSeason(-25, 2);
        sleep(2000);
        //        chassis.encoderDriveFirstThirdAccel(1.0,90, 10);
    }

    class SliderThread implements Runnable {
        @Override
        public void run() {
            switch (pos) {
                case LEFT:
                    robot.slider.setTargetPosition(-950);
                    break;
                case RIGHT:
                    robot.slider.setTargetPosition(-2400);
                    break;
                case NOT:
                    robot.slider.setTargetPosition(-3900);
            }
            robot.slider.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            robot.slider.setPower(1.0);
            while (robot.slider.getCurrentPosition() > -400) {

            }
            robot.bucket.setPosition(0.3);
            while (!goDown) {

            }
            if (robot.slider.getCurrentPosition() > -1500) {
                robot.bucket.setPosition(0);
                ElapsedTime runtimeThread = new ElapsedTime();
                runtimeThread.reset();
                while(runtimeThread.seconds() < 1.5) {

                }
            }
            robot.slider.setPower(0.75);
            robot.slider.setTargetPosition(0);
            while (robot.slider.getCurrentPosition() < -1500) {

            }
            robot.bucket.setPosition(0);
        }
    }
}
