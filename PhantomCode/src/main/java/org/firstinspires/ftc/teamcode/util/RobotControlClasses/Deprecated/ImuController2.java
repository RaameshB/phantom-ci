package org.firstinspires.ftc.teamcode.util.RobotControlClasses.Deprecated;

import com.qualcomm.hardware.bosch.BNO055IMU;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;
import org.firstinspires.ftc.teamcode.util.RobotConfig;

@Deprecated
public class ImuController2 {

    RobotConfig hardware;
    LinearOpMode ln;
    Orientation lastAngles;

    public ImuController2(RobotConfig robot, LinearOpMode linearOpMode) {
        hardware = robot;
        ln = linearOpMode;
    }

    /**
     * From 0 to 360
     */
    private double globalAngle = 0;
    /**
     * From -180 to 180
     */
    private double relativeAngle = 0;

    public void updateAngle()
    {
        // We experimentally determined the Z axis is the axis we want to use for heading angle.
        // We have to process the angle because the imu works in euler angles so the Z axis is
        // returned as 0 to +180 or 0 to -180 rolling back to -179 or +179 when rotation passes
        // 180 degrees. We detect this transition and track the total cumulative angle of rotation.

        Orientation angles = hardware.imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);

        double deltaAngle = angles.firstAngle - lastAngles.firstAngle;

        if (deltaAngle < -180)
            deltaAngle += 360;
        else if (deltaAngle > 180)
            deltaAngle -= 360;

        globalAngle += deltaAngle;
        relativeAngle += deltaAngle;

        if (globalAngle > 360) {
            do {
                globalAngle -= 360;
            } while (globalAngle > 360);
            if (globalAngle > 180) {
                globalAngle = 180 - globalAngle;
            }
        } else if (globalAngle < 0) {
            do {
                globalAngle += 360;
            } while (globalAngle < 0);
        }

        if (relativeAngle > 360) {
            do {
                relativeAngle -= 360;
            } while (relativeAngle > 360);
            if (relativeAngle > 180) {
                relativeAngle= 180 - relativeAngle;
            }
        } else if (relativeAngle < -360) {
            do {
                relativeAngle += 360;
            } while (relativeAngle < -360);
            if (relativeAngle < -180) {
                relativeAngle = -180 - relativeAngle;
            }
        }

        lastAngles = angles;
    }

    public double getRelativeAngle() {
        updateAngle();
        return relativeAngle;
    }

    public void resetRelativeAngle() {
        relativeAngle = 0;
    }

    public double getGlobalAngle() {
        updateAngle();
        return globalAngle;
    }
}
