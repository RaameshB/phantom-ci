package org.firstinspires.ftc.teamcode.util.visionDev.easyopencv.oldJavaPipelines;

import org.firstinspires.ftc.teamcode.util.enums.Positions;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.openftc.easyopencv.OpenCvPipeline;

// TODO: Make this less scuffed, like why the heck does this work
public class PipelineMkSevenRed extends OpenCvPipeline {

    Positions pos;

    {
        pos = Positions.stillProc;
    }

    static final double PERCENT_COLOUR_THRESHOLD_MID = 0.17;
    static final double PERCENT_COLOUR_THRESHOLD_RIGHT = 0.285;

    class Mid_ROI {
        public int x1 = 0;
        public int y1 = 40;
        public int x2 = 57;
        public int y2 = 78;
        public Point startPoint = new Point(x1, y1);
        public Point endPoint = new Point(x2, y2);
    }
    Mid_ROI midRoi = new Mid_ROI();
    class RIGHT_ROI {
        public int x1 = 114;
        public int y1 = 39;
        public int x2 = 183;
        public int y2 = 77;
        public Point startPoint = new Point(x1, y1);
        public Point endPoint = new Point(x2, y2);
    }
    RIGHT_ROI rightRoi = new RIGHT_ROI();

    Scalar lowerYellow = new Scalar(20, 80, 130);
    Scalar upperYellow = new Scalar(60, 255, 255);

    Scalar colorBlue = new Scalar(0, 0, 255);
    Scalar colorRed = new Scalar(255, 0 , 0);
    Scalar colorGreen = new Scalar(0, 255, 0);

    int thickness = 2;

    // TODO: Figure out why the heck this works
    final double aCompletelyArbitraryValueThatMayFixThings = 2.7717391304347825785680529300567111025211638037314010595525673507456406570373114049386885104640064680577335819129731732855045444242046382010375606064713239017656455818334358503734175887670016263758958;

    public int rectArea(int x1, int x2, int y1, int y2) {
        return (x2 - x1) * (y2 - y1);
    }

    double rightValue = 0;// = Core.sumElems(leftSubmat).val[0] / rectArea(leftRoi.x1, leftRoi.x2, leftRoi.y1, leftRoi.y2) / 255 * aCompletelyArbitraryValueThatMayFixThings;
    double midValue = 0;

    @Override
    public Mat processFrame(Mat input) {
        Mat hsvImg = new Mat();
        Imgproc.cvtColor(input, hsvImg, Imgproc.COLOR_BGR2HSV);

//        Mat inRangeMat = new Mat();
//        Core.inRange(hsvImg, lowerYellow, upperYellow, inRangeMat);

        Mat midSubmat = hsvImg.submat(midRoi.y1, midRoi.y2, midRoi.x1, midRoi.x2);

        Mat midMask = new Mat(midSubmat.rows(), midSubmat.cols(), CvType.CV_8U, new Scalar(0));

        Core.inRange(midSubmat, lowerYellow, upperYellow, midMask);

        midSubmat.release();

        midValue = Core.sumElems(midMask).val[0] / rectArea(midRoi.x1, midRoi.x2, midRoi.y1, midRoi.y2) / 255;

        midMask.release();

        Mat rightSubmat = hsvImg.submat(rightRoi.y1, rightRoi.y2, rightRoi.x1, rightRoi.x2);

        Mat rightMask = new Mat(rightSubmat.rows(), rightSubmat.cols(), CvType.CV_8U, new Scalar(0));
//

        Core.inRange(rightSubmat, lowerYellow, upperYellow, rightMask);

        Imgproc.cvtColor(rightMask, rightMask, Imgproc.COLOR_HSV2BGR);
        Imgproc.cvtColor(rightMask, rightMask, Imgproc.COLOR_BGR2GRAY);

        rightSubmat.release();

        rightValue = Core.sumElems(rightMask).val[0] / rectArea(rightRoi.x1, rightRoi.x2, rightRoi.y1, rightRoi.y2) / 255;

        rightMask.release();

        if (rightValue > PERCENT_COLOUR_THRESHOLD_RIGHT) {
            pos = Positions.rightTop;
        } else if (midValue > PERCENT_COLOUR_THRESHOLD_MID) {
            pos = Positions.midMid;
        } else {
            pos = Positions.leftBot;
        }

        Mat rectMat = input;
        if (pos == Positions.rightTop) {
            Imgproc.rectangle(rectMat, rightRoi.startPoint, rightRoi.endPoint, colorBlue, thickness);
            Imgproc.rectangle(rectMat, midRoi.startPoint, midRoi.endPoint, colorRed, thickness);
        } else if (pos == Positions.midMid) {
            Imgproc.rectangle(rectMat, rightRoi.startPoint, rightRoi.endPoint, colorRed, thickness);
            Imgproc.rectangle(rectMat, midRoi.startPoint, midRoi.endPoint, colorBlue, thickness);
        } else {
            Imgproc.rectangle(rectMat, rightRoi.startPoint, rightRoi.endPoint, colorGreen, thickness);
            Imgproc.rectangle(rectMat, midRoi.startPoint, midRoi.endPoint, colorGreen, thickness);
        }

        return rectMat;
    }

    public Positions getPos() {
        return pos;
    }
    public double getRightValue() {
        return rightValue;
    }
    public double getMidValue() {
        return  midValue;
    }
}
