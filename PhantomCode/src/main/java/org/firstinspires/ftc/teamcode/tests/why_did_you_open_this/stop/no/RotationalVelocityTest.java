package org.firstinspires.ftc.teamcode.tests.why_did_you_open_this.stop.no;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.robotcore.external.navigation.AngularVelocity;
import org.firstinspires.ftc.teamcode.util.RobotConfig;

@Disabled
@TeleOp
public class RotationalVelocityTest extends LinearOpMode {

    AngularVelocity angularVelocity;

    @Override
    public void runOpMode() throws InterruptedException {
        RobotConfig robot = new RobotConfig();
        robot.init(hardwareMap, this);
        waitForStart();
        robot.rightDrive.setPower(-0.5);
        robot.leftDrive.setPower(0.5);
        while (opModeIsActive()) {
            angularVelocity = robot.imu.getAngularVelocity();
            telemetry.addData("Z Rotation Rate: ", angularVelocity.zRotationRate);
            telemetry.addData("Y Rotation Rate: ", angularVelocity.yRotationRate);
            telemetry.addData("X Rotation Rate: ", angularVelocity.xRotationRate);
            telemetry.update();
            idle();
        }
        robot.rightDrive.setPower(0);
        robot.leftDrive.setPower(0);
    }
}
