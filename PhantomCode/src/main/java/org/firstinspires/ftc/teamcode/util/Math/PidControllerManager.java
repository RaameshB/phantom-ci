package org.firstinspires.ftc.teamcode.util.Math;

import com.stormbots.MiniPID;

public class PidControllerManager {

    public PidControllerManager() {
        pid.setOutputLimits(-1.0, 1.0);
        pid.setMaxIOutput(0.4);
        pid.setDirection(false);
        pid.setSetpointRange(360);
    }

    MiniPID pid = new MiniPID(0, 0 ,0, 0);

    public void setTargetAngle(double angle) {
        pid.setSetpoint(angle);
    }

    public double getOutput() {
        return pid.getOutput();
    }


}
