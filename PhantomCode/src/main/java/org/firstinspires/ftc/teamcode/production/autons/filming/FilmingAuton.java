package org.firstinspires.ftc.teamcode.production.autons.filming;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.teamcode.util.RobotConfig;
import org.firstinspires.ftc.teamcode.util.RobotControlClasses.ChassisController3;
import org.firstinspires.ftc.teamcode.util.RobotControlClasses.CvWebcamController;
import org.firstinspires.ftc.teamcode.util.RobotControlClasses.CvWebcamController2Red;
import org.firstinspires.ftc.teamcode.util.RobotControlClasses.ImuController3;

@Disabled
@Autonomous
public class FilmingAuton extends LinearOpMode {
    
    RobotConfig robot = new RobotConfig();
    ImuController3 imuController;
    ChassisController3 chassis;
    CvWebcamController2Red cvWebcam;
    
    @Override
    public void runOpMode() throws InterruptedException {
        robot.initButItWasBetter(hardwareMap, this);
        imuController = new ImuController3(robot, this);
        chassis = new ChassisController3(robot, this, imuController);
        waitForStart();
//        telemetry.addLine("Phase: Acceleration");
//        telemetry.update();
//        while (!isStopRequested() || !gamepad1.b) {
//            while (!isStopRequested() && !gamepad1.a) {
//                if (gamepad1.b) {
//                    break;
//                }
//                idle();
//            }
//            chassis.encoderDrive(30, 2);
//            while (!isStopRequested() && gamepad1.a) {
//                if (gamepad1.b) {
//                    break;
//                }
//                idle();
//            }
//            while (!isStopRequested() && !gamepad1.a) {
//                if (gamepad1.b) {
//                    break;
//                }
//                idle();
//            }
//            chassis.encoderDrive(10, 2);
//            while (!isStopRequested() && gamepad1.a) {
//                if (gamepad1.b) {
//                    break;
//                }
//                idle();
//            }
//        }
//        while (!isStopRequested() || gamepad2.b) {
//            idle();
//        }
        telemetry.addLine("Phase: Turn");
        telemetry.update();
        while (!isStopRequested() || !gamepad1.b) {
            while (!isStopRequested() && !gamepad1.a) {
                if (gamepad1.b) {
                    break;
                }
                idle();
            }
            chassis.pointUsingImuGlobalAngle(90, ChassisController3.Direction.clockwise, 3);
            while (!isStopRequested() && gamepad1.a) {
                if (gamepad1.b) {
                    break;
                }
                idle();
            }
        }
        while (!isStopRequested() || gamepad2.b) {
            idle();
        }
    }
}
