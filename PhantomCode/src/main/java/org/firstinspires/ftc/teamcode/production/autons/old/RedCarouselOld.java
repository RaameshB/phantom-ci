package org.firstinspires.ftc.teamcode.production.autons.old;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;

import org.firstinspires.ftc.teamcode.util.RobotConfig;
import org.firstinspires.ftc.teamcode.util.RobotControlClasses.CVC3;
import org.firstinspires.ftc.teamcode.util.RobotControlClasses.ChassisController3;
import org.firstinspires.ftc.teamcode.util.RobotControlClasses.ImuController3;
import org.firstinspires.ftc.teamcode.util.enums.LeftAndRightEnum;

@Disabled
@Autonomous(group = "redForComp", preselectTeleOp = "RedTeleOp")
public class RedCarouselOld extends LinearOpMode {

    RobotConfig robot;
    ImuController3 imuController;
    ChassisController3 chassis;
    CVC3 cvWebcam;

    LeftAndRightEnum pos;

    boolean bot = false;

    @Override
    public void runOpMode() throws InterruptedException {
        robot = new RobotConfig();

        robot.initButItWasBetter(hardwareMap, this);

        imuController = new ImuController3(robot, this);
        chassis = new ChassisController3(robot, this, imuController);
        cvWebcam = new CVC3(robot, this);
        cvWebcam.startAsyncStream();

        telemetry.addLine("Opening Camera...");
        telemetry.update();

        imuController.resetRelativeAngle();

        robot.intakeRotationServo.setPosition(0.7);

        while (cvWebcam.getPos() == LeftAndRightEnum.STILL_PROC && !isStopRequested()) {
            idle();
        }
        sleep(500);
        telemetry.addLine("Waiting For Start...");
        telemetry.update();
        while (!isStarted() && !isStopRequested()) {
            idle();
        }
        waitForStart();
        pos = cvWebcam.getPos();
        cvWebcam.stopStream();
        switch (pos) {
            case NOT:
                robot.slider.setTargetPosition(-637);
                bot = true;
                break;
            case  LEFT:
                robot.slider.setTargetPosition(-2236);
                break;
            case RIGHT:
                robot.slider.setTargetPosition(-3200);
        }
        robot.slider.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        robot.slider.setPower(1.0);
        chassis.encoderDrive(5, 2);
        robot.bucket.setPosition(0.5);
        chassis.pointUsingImuGlobalAngle(34.0, ChassisController3.Direction.clockwise, 2.5);
        chassis.encoderDrive(21.0, 4);
        robot.bucket.setPosition(1.0);
        sleep(1000);
        if (!bot) {
            robot.bucket.setPosition(0.50);
            robot.slider.setPower(0.35);
            robot.slider.setTargetPosition(0);
        } else {
            robot.bucket.setPosition(0);
        }
        chassis.encoderDrive(-10, 3);
        if (bot) {
            robot.slider.setTargetPosition(0);
        } else {
            robot.bucket.setPosition(0);
        }
        chassis.pointUsingImuGlobalAngle(270, ChassisController3.Direction.clockwise, 2.5);
        chassis.encoderDrive(-20, 3);
        chassis.imuTurnFromLastSeason(-60, 2.0);
        chassis.encoderDrive(-16.25, 3);
        robot.carousel.setPower(-0.550);
        sleep(2750);
        robot.carousel.setPower(0);
        chassis.encoderDrive(20, 2);
        chassis.pointUsingImuGlobalAngle(90, ChassisController3.Direction.clockwise, 2.5);
        chassis.encoderDriveFirstThirdAccel(1.0,90, 10);
    }
}
