import cv2
from cv2 import COLOR_BGR2RGB
import matplotlib.pyplot as plt

img = cv2.imread("testImages/yellow1.jpg")

img = cv2.cvtColor(img, COLOR_BGR2RGB)

plt.imshow(img)
plt.show()