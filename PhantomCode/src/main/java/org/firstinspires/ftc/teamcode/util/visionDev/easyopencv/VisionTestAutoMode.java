package org.firstinspires.ftc.teamcode.util.visionDev.easyopencv;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;
//import org.firstinspires.ftc.teamcode.util.JAVA_CV.Locations;
//import org.firstinspires.ftc.teamcode.util.JAVA_CV;
import org.firstinspires.ftc.teamcode.util.RobotConfig;
//import org.firstinspires.ftc.teamcode.util.RobotControl.SliderController;
import org.firstinspires.ftc.teamcode.util.visionDev.easyopencv.oldJavaPipelines.Deprecated.JAVA_CV;
import org.openftc.easyopencv.OpenCvCamera;
import org.openftc.easyopencv.OpenCvCameraFactory;
import org.openftc.easyopencv.OpenCvCameraRotation;

@Disabled
@Autonomous(name = "VTAM", group = "LELELELELELLELELELEELEL")
public class VisionTestAutoMode extends LinearOpMode {
    RobotConfig robot = new RobotConfig();






    private ElapsedTime runtime = new ElapsedTime();

    OpenCvCamera camera;

    boolean left = false;
    boolean right = false;
    boolean other = false;

    @Override
    public void runOpMode() throws InterruptedException {
        //camera init
        int cameraMonitorViewId = hardwareMap.appContext.getResources().getIdentifier("cameraMonitorViewId", "id", hardwareMap.appContext.getPackageName());
        camera = OpenCvCameraFactory.getInstance().createWebcam(hardwareMap.get(WebcamName.class , "Webcam 1"), cameraMonitorViewId);

        JAVA_CV detector = new JAVA_CV(telemetry);
        camera.setPipeline(detector);
//        camera.openCameraDeviceAsync(
//                () -> camera.startStreaming(1280 , 720 , OpenCvCameraRotation.UPRIGHT)
//        );


        camera.openCameraDeviceAsync(new OpenCvCamera.AsyncCameraOpenListener() {
            @Override
            public void onOpened() {
                /*
                 * Tell the webcam to start streaming images to us! Note that you must make sure
                 * the resolution you specify is supported by the camera. If it is not, an exception
                 * will be thrown.
                 *
                 * Keep in mind that the SDK's UVC driver (what OpenCvWebcam uses under the hood) only
                 * supports streaming from the webcam in the uncompressed YUV image format. This means
                 * that the maximum resolution you can stream at and still get up to 30FPS is 480p (640x480).
                 * Streaming at e.g. 720p will limit you to up to 10FPS and so on and so forth.
                 *
                 * Also, we specify the rotation that the webcam is used in. This is so that the image
                 * from the camera sensor can be rotated such that it is always displayed with the image upright.
                 * For a front facing camera, rotation is defined assuming the user is looking at the screen.
                 * For a rear facing camera or a webcam, rotation is defined assuming the camera is facing
                 * away from the user.
                 */
                camera.startStreaming(1280, 720, OpenCvCameraRotation.UPRIGHT);
            }

            @Override
            public void onError(int errorCode) {

            }
        });







        waitForStart();
        while (opModeIsActive()){
            telemetry.addData("Location:" , detector.getLocation());
            telemetry.update();

        }







        camera.stopStreaming();
    }
}