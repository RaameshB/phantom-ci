package org.firstinspires.ftc.teamcode.tests;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.util.RobotConfig;
import org.firstinspires.ftc.teamcode.util.RobotControlClasses.AiCamController;

@Disabled
@TeleOp
public class aiTester extends LinearOpMode {

    RobotConfig robot = new RobotConfig();
    AiCamController aiCam;

    @Override
    public void runOpMode() throws InterruptedException {
        robot.webcamOnly(hardwareMap, this);
        aiCam = new AiCamController(robot, this, hardwareMap, hardwareMap.appContext);
        aiCam.startAsyncStream();
        waitForStart();
        while(!isStopRequested()) {
            telemetry.addData("procTime: ", aiCam.getProcTime());
            telemetry.addData("outputs: ", aiCam.getOutputs());
            telemetry.update();
            idle();
        }

    }
}
