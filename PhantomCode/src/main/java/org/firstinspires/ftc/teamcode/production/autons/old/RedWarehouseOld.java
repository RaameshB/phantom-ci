package org.firstinspires.ftc.teamcode.production.autons.old;

import  com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.util.RobotConfig;

import org.firstinspires.ftc.teamcode.util.RobotControlClasses.ChassisController3;
import org.firstinspires.ftc.teamcode.util.RobotControlClasses.CvWebcamController;
import org.firstinspires.ftc.teamcode.util.RobotControlClasses.CvWebcamController2Red;
import org.firstinspires.ftc.teamcode.util.RobotControlClasses.Deprecated.ImuController;
import org.firstinspires.ftc.teamcode.util.RobotControlClasses.Deprecated.SliderController;
import org.firstinspires.ftc.teamcode.util.RobotControlClasses.ImuController3;
import org.firstinspires.ftc.teamcode.util.enums.Positions;

@Disabled
@Autonomous(group = "redForComp", preselectTeleOp = "RedTeleOp")
public class RedWarehouseOld extends LinearOpMode {

    static final double COUNTS_PER_REVOLUTION = 1120;
    static final double SPROCKET_REDUCTION = 1.5;
    static final double GEAR_REDUCTION = 0.5;
    static final double WHEEL_DIAMETER_INCHES = 4;
    static final double COUNTS_PER_INCH = (COUNTS_PER_REVOLUTION * SPROCKET_REDUCTION * GEAR_REDUCTION) / (WHEEL_DIAMETER_INCHES * Math.PI);

    private ElapsedTime runtime = new ElapsedTime();
    Positions pos;
    RobotConfig robot;
    ImuController3 imuController;
    ChassisController3 chassis;
    CvWebcamController2Red cvWebcam;

    boolean bot = false;

    // 0, -287, -669
    @Override
    public void runOpMode() throws InterruptedException {
        robot = new RobotConfig();

        robot.initButItWasBetter(hardwareMap, this);

//        telemetry.addLine("Init Complete");

//        sleep(2000);

        imuController = new ImuController3(robot, this);
        chassis = new ChassisController3(robot, this, imuController);
        cvWebcam = new CvWebcamController2Red(robot);
        cvWebcam.startAsyncStream();

        telemetry.addLine("Opening Camera...");
        telemetry.update();

        imuController.resetRelativeAngle();

        while (cvWebcam.getPos() == Positions.stillProc && !isStopRequested()) {
            idle();
        }
        sleep(500);
        telemetry.addLine("Waiting For Start...");
        telemetry.update();
        while (!isStarted() && !isStopRequested()) {
            idle();
        }
        waitForStart();
        pos = cvWebcam.getPos();
        cvWebcam.stopStream();

        switch (pos) {
            case midMid:
                robot.slider.setTargetPosition(-637);
                bot = true;
                break;
            case  rightTop:
                robot.slider.setTargetPosition(-2236);
                break;
            case leftBot:
                robot.slider.setTargetPosition(-3200);
        }
        robot.slider.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        robot.slider.setPower(1.0);
        telemetry.addData("pos",pos);
        chassis.encoderDrive(4,5);
        robot.bucket.setPosition(0.5);
        chassis.pointUsingImuGlobalAngle(360-30, ChassisController3.Direction.counterclockwise, 1.0);
        chassis.encoderDrive(22.5,4);
        robot.bucket.setPosition(1.0);
        sleep(1000);
        if (!bot) {
            robot.bucket.setPosition(0.50);
            robot.slider.setPower(0.35);
            robot.slider.setTargetPosition(0);
        } else {
            robot.bucket.setPosition(0);
        }
        chassis.encoderDrive(-10,4);
        if (bot) {
            robot.slider.setTargetPosition(0);
        } else {
            robot.bucket.setPosition(0);
        }
        chassis.pointUsingImuGlobalAngle(265,ChassisController3.Direction.counterclockwise, 2.5);
        chassis.encoderDriveFirstThirdAccel(1.0,-37,4);

    }


}
