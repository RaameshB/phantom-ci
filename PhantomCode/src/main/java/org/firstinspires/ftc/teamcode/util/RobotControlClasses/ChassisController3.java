package org.firstinspires.ftc.teamcode.util.RobotControlClasses;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.checkerframework.checker.units.qual.A;
import org.firstinspires.ftc.robotcore.external.Func;
import org.firstinspires.ftc.robotcore.external.Function;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;
import org.firstinspires.ftc.teamcode.util.Math.MiscFuncs;
import org.firstinspires.ftc.teamcode.util.RobotConfig;
import org.opencv.core.Mat;

import java.util.ArrayList;

public class ChassisController3 {

    private static final double MIN_DRIVE_POWER = 0.1;
    private static final boolean IS_DIRECTION_REVERSED = false;

    public double currentDeltaAngle = 0;

    private double getAbsRelativeAngle() {
        return Math.abs(imuController.getRelativeAngle());
    }

    double gain2 = 0.02;

    public void imuTurnButItsFormattedForTheControlAward (double angle, double maxTime) {
        double oldGain = gain2;
        //Angle from the start of the turn
        imuController.relativeAngle = angle;
        ElapsedTime time = new ElapsedTime();
        time.reset();
        double x = 0;
        while (!ln.isStopRequested() && time.seconds() <= maxTime) {
            //checkDirection returns the negative of the angular error multiplied by a calibrated gain
            //rotate rotates the robot, positive power is clockwise, negative power is counterclockwise
            rotate(checkDirection());
            //steadily increases checkDirection() gain to get out of stall conditions based on sensor changing sensor update intervals
            x += 0.05;
            if (time.seconds() > x) { gain2 += 0.005; }
        }
        rotate(0);
        //resets checkDirection() gain to its original value
        gain2 = oldGain;
    }


    public void imuTurnFromLastSeason (double angle, double maxTime) {

        double oldGain = gain2;

        imuController.relativeAngle = angle;

        ElapsedTime time = new ElapsedTime();

        time.reset();

        double x = 0;

        while (!ln.isStopRequested() && time.seconds() <= maxTime) {

            rotate(checkDirection());
            x += 0.05;
            if (time.seconds() > x) {
                gain2 += 0.005;
            }
        }

        rotate(0);

        gain2 = oldGain;
    }
    private double checkDirection()
    {
        // The gain value determines how sensitive the correction is to direction changes.
        // You will have to experiment with your robot to get small smooth direction changes
        // to stay on a straight line.
        double correction;
        //double gain = 0.025;

        //gain = mult * angle;

        correction = -imuController.getRelativeAngle() * gain2;        // reverse sign of angle for correction.

        return correction;
    }

    @Deprecated
    public void turnUsingImuSimple(double angle) {

        if (angle < 4 && angle > -4) {
            imuController.rotate((int) angle, 0.1);
            return;
        }

        currentDeltaAngle = angle;
        double deltaAngle = angle;
        double absDeltaAngle = Math.abs(deltaAngle);

        if (deltaAngle > 0) {
            rotate(1.0);
        } else if (deltaAngle < 0) {
            rotate(-1.0);
        } else {
            return;
        }

        double neededChangeInAng;
        double rotatePower;
        double gain = 0.008;

        neededChangeInAng = deltaAngle - imuController.getRelativeAngle();
        rotatePower = neededChangeInAng * gain;

//        ArrayList<Double> averagingArray = new ArrayList<Double>();

//        int i = 0;
//        int i2;
//
//        double sum;
//        double avg = 2;

        while ((Math.abs(hardware.imu.getAngularVelocity().yRotationRate) > 5 || Math.abs(rotatePower) > 0.1 || Math.abs(neededChangeInAng) > 3 /* || Math.abs(avg) > 1 */) && ln.opModeIsActive()) {

//            if (averagingArray.size() < 6) {
//                averagingArray.add(neededChangeInAng);
//            } else {
//                if (i == 6) {
//                    i = 0;
//                }
//                averagingArray.set(i, neededChangeInAng);
//            }
//
//            i2 = 0;
//            sum = 0;
//            while (i < averagingArray.size()) {
//                sum += averagingArray.get(i);
//                i++;
//            }
//            avg = sum/i;

            neededChangeInAng = deltaAngle - imuController.getRelativeAngle();
            rotatePower = neededChangeInAng * gain;
            ln.telemetry.addLine("correction turn");
            ln.telemetry.addData("deltaAngle", deltaAngle);
            ln.telemetry.addData("neededChangeInAngle", neededChangeInAng);
            ln.telemetry.addData("AngVeloc: ", hardware.imu.getAngularVelocity().yRotationRate);
            ln.telemetry.addData("Rotate Power: ", rotatePower);
            ln.telemetry.update();
            rotate(rotatePower);
        }

        rotate(0);

    }

    @Deprecated
    public void turnUsingImu(double angle) {

        if (angle < 4 && angle > -4) {
            imuController.rotate((int) angle, 0.1);
            return;
        }

        currentDeltaAngle = angle;
        double deltaAngle = angle;
        double absDeltaAngle = Math.abs(deltaAngle);

        imuController.resetRelativeAngle();

        // Get off of the weird Zero Pos (prevents the robot from doing weird things)
        double percentCompleteAbs = Math.abs(imuController.getRelativeAngle()) / deltaAngle;

        ln.telemetry.addLine("InitRotation");
        ln.telemetry.update();

        if (currentDeltaAngle > 0) {
            while (getAbsRelativeAngle() < 4 && ln.opModeIsActive()) {
                rotate(Math.max(0.15,MiscFuncs.oneThirdTwoThirdEasingSine(percentCompleteAbs)));
                percentCompleteAbs = getAbsRelativeAngle() / deltaAngle;
            }
        } else if (currentDeltaAngle < 0) {
            while (getAbsRelativeAngle() < 4 && ln.opModeIsActive()) {
                rotate(Math.min(-0.15,MiscFuncs.oneThirdTwoThirdEasingSine(-percentCompleteAbs)));
                percentCompleteAbs = getAbsRelativeAngle() / deltaAngle;
            }
        } else {
            return;
        }

//        rotate(0);
//        ln.sleep(2000);

        //Actual Turn Stuff
        double percentCompleteWithDirection = imuController.getRelativeAngle() / absDeltaAngle;
        double powerPercent;
        double relativeAngle;

        double maxPower = Math.abs(deltaAngle / 200);

        if (maxPower > 1) {
            maxPower = 1;
        }

        MiscFuncs.easingFuncs easingFunc = MiscFuncs.easingFuncs.oneThirdTwoThirdEasingSine;

        while (percentCompleteAbs < 1.0 && ln.opModeIsActive()) {
            relativeAngle = imuController.getRelativeAngle();
            percentCompleteAbs = relativeAngle / deltaAngle;

//            if ( (deltaAngle < 0 && relativeAngle < deltaAngle) || (deltaAngle > 0 && relativeAngle > deltaAngle) ) {
//                percentCompleteWithDirection = (deltaAngle - relativeAngle) / absDeltaAngle;
//                easingFunc = MiscFuncs.easingFuncs.scuffedHalfEasingSine;
//                ln.telemetry.addLine("0:0");
//            } else {
            percentCompleteWithDirection = relativeAngle / absDeltaAngle;
            ln.telemetry.addLine("0:1");
//            }


            powerPercent = MiscFuncs.easingFuncs(percentCompleteWithDirection * maxPower, easingFunc);
//            if (percentCompleteWithDirection > 0) {
//                powerPercent = MiscFuncs.easingFuncs((percentCompleteWithDirection) * maxPower, easingFunc);
//                ln.telemetry.addLine("1:0");
//            } else {
//                powerPercent = MiscFuncs.easingFuncs((percentCompleteWithDirection) * maxPower, easingFunc);
//                ln.telemetry.addLine("1:1");
//            }

            rotate(powerPercent);
            ln.telemetry.addData("deltaAngle", deltaAngle);
            ln.telemetry.addData("angle", relativeAngle);
            ln.telemetry.addData("power: ", powerPercent);
            ln.telemetry.addData("percentComplete: ", percentCompleteAbs);
            ln.telemetry.addData("AngVeloc: ", hardware.imu.getAngularVelocity().yRotationRate);
            ln.telemetry.update();
        }

        double neededChangeInAng;
        double rotatePower;
        double gain = 0.01;

//        ln.telemetry.update();

        neededChangeInAng = deltaAngle - imuController.getRelativeAngle();
        rotatePower = neededChangeInAng * gain;
        while ((Math.abs(hardware.imu.getAngularVelocity().yRotationRate) > 5 || Math.abs(rotatePower) > 0.1 || Math.abs(neededChangeInAng) > 1) && ln.opModeIsActive()) {
            neededChangeInAng = deltaAngle - imuController.getRelativeAngle();
            rotatePower = neededChangeInAng * gain;
            ln.telemetry.addLine("correction turn");
            ln.telemetry.addData("deltaAngle", deltaAngle);
            ln.telemetry.addData("neededChangeInAngle", neededChangeInAng);
            ln.telemetry.addData("AngVeloc: ", hardware.imu.getAngularVelocity().yRotationRate);
            ln.telemetry.addData("Rotate Power: ", rotatePower);
            ln.telemetry.update();
            rotate(rotatePower);
        }

        rotate(0);
    }


    public void pointUsingImuGlobalAngle(double angle, Direction direction, double maxTime) {
        double currentAngle = imuController.getGlobalAngle();
        double deltaAngle;
        if (direction == Direction.clockwise && currentAngle > angle) {
            deltaAngle = (360 - currentAngle) + angle;
        } else if (direction == Direction.counterclockwise && currentAngle < angle) {
            deltaAngle = -((360 - currentAngle) + angle);
        } else {
            deltaAngle = angle - currentAngle;
        }
        imuTurnFromLastSeason(deltaAngle, maxTime);
    }

    static final double COUNTS_PER_REVOLUTION = 1120;
    static final double SPROCKET_REDUCTION = 1.5;
    static final double GEAR_REDUCTION = 0.5;
    static final double WHEEL_DIAMETER_INCHES = 4;
    static final double COUNTS_PER_INCH = (COUNTS_PER_REVOLUTION * SPROCKET_REDUCTION * GEAR_REDUCTION) / (WHEEL_DIAMETER_INCHES * Math.PI);

    public void encoderDrive(double maxPower, double distance, double timeoutS) {

        hardware.rightDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        hardware.leftDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

        // Turn off RUN_TO_POSITION
        hardware.leftDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        hardware.rightDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        int newLeftTarget;
        int newRightTarget;

        int originalLeft;
        int originalRight;
        int originalLeftAbs;
        int originalRightAbs;

        ElapsedTime runtime = new ElapsedTime();

        // Ensure that the opmode is still active
        if (ln.opModeIsActive()) {

            // Determine new target position, and pass to motor controller

            int distToCounts = (int)(distance * COUNTS_PER_INCH);
            int absDistToCounts = Math.abs(distToCounts);

            originalLeft = hardware.leftDrive.motor1.getCurrentPosition();
            originalRight = hardware.rightDrive.motor1.getCurrentPosition();
            originalLeftAbs = Math.abs(originalLeft);
            originalRightAbs = Math.abs(originalRight);
            newLeftTarget = hardware.leftDrive.motor1.getCurrentPosition() + distToCounts;
            newRightTarget = hardware.rightDrive.motor1.getCurrentPosition() + distToCounts;
            hardware.leftDrive.setTargetPosition(newLeftTarget);
            hardware.rightDrive.setTargetPosition(newRightTarget);


            // Turn On RUN_TO_POSITION
            hardware.leftDrive.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            hardware.rightDrive.setMode(DcMotor.RunMode.RUN_TO_POSITION);

            // reset the timeout time and start motion.
            runtime.reset();

            // keep looping while we are still active, and there is time left, and both motors are running.
            // Note: We use (isBusy() && isBusy()) in the loop test, which means that when EITHER motor hits
            // its target position, the motion will stop.  This is "safer" in the event that the robot will
            // always end the motion as soon as possible.
            // However, if you require that BOTH motors have finished their moves before the robot continues
            // onto the next step, use (isBusy() || isBusy()) in the loop test.

            double percentComplete;
            double speed;
            while (ln.opModeIsActive() &&
                    (runtime.seconds() < timeoutS) &&
                    (hardware.leftDrive.motor1.isBusy() && hardware.rightDrive.motor1.isBusy())) {

                percentComplete = MiscFuncs.average(Math.abs(Math.abs(hardware.leftDrive.motor1.getCurrentPosition()) - originalLeftAbs)/(double) absDistToCounts, Math.abs(Math.abs(hardware.rightDrive.motor1.getCurrentPosition()) - originalRightAbs)/(double) absDistToCounts);
                if (percentComplete > 1) {
                    percentComplete -= 1;
                }
                speed = Math.max(0.1,Math.abs(MiscFuncs.oneThirdTwoThirdEasingSine(percentComplete) * maxPower));
                hardware.leftDrive.setPower(speed);
                hardware.rightDrive.setPower(speed);
                // Display it for the driver.
                ln.telemetry.addData("Path1",  "Running to %7d :%7d", newLeftTarget,  newRightTarget);
                ln.telemetry.addData("Path2",  "Running at %7d :%7d",
                        hardware.leftDrive.motor1.getCurrentPosition(),
                        hardware.rightDrive.motor1.getCurrentPosition());
                ln.telemetry.update();
            }

            // Stop all motion;
            hardware.leftDrive.setPower(0);
            hardware.rightDrive.setPower(0);

            hardware.rightDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
            hardware.leftDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

            // Turn off RUN_TO_POSITION
            hardware.leftDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            hardware.rightDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

            //  sleep(250);   // optional pause after each move
        }
    }

    public void encoderDrive(double distance, double timeoutS) {

        hardware.rightDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        hardware.leftDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

        // Turn off RUN_TO_POSITION
        hardware.leftDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        hardware.rightDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        int newLeftTarget;
        int newRightTarget;

        int originalLeft;
        int originalRight;
        int originalLeftAbs;
        int originalRightAbs;

        ElapsedTime runtime = new ElapsedTime();

        // Ensure that the opmode is still active
        if (ln.opModeIsActive()) {

            // Determine new target position, and pass to motor controller

            int distToCounts = (int)(distance * COUNTS_PER_INCH);
            int absDistToCounts = Math.abs(distToCounts);

            originalLeft = hardware.leftDrive.motor1.getCurrentPosition();
            originalRight = hardware.rightDrive.motor1.getCurrentPosition();
            originalLeftAbs = Math.abs(originalLeft);
            originalRightAbs = Math.abs(originalRight);
            newLeftTarget = hardware.leftDrive.motor1.getCurrentPosition() + distToCounts;
            newRightTarget = hardware.rightDrive.motor1.getCurrentPosition() + distToCounts;
            hardware.leftDrive.setTargetPosition(newLeftTarget);
            hardware.rightDrive.setTargetPosition(newRightTarget);


            // Turn On RUN_TO_POSITION
            hardware.leftDrive.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            hardware.rightDrive.setMode(DcMotor.RunMode.RUN_TO_POSITION);

            // reset the timeout time and start motion.
            runtime.reset();

            // keep looping while we are still active, and there is time left, and both motors are running.
            // Note: We use (isBusy() && isBusy()) in the loop test, which means that when EITHER motor hits
            // its target position, the motion will stop.  This is "safer" in the event that the robot will
            // always end the motion as soon as possible.
            // However, if you require that BOTH motors have finished their moves before the robot continues
            // onto the next step, use (isBusy() || isBusy()) in the loop test.
            double maxPower = Math.abs(distance) / 20;
            if (maxPower > 1) {
                maxPower = 1;
            }
            double percentComplete;
            double speed;
            while (ln.opModeIsActive() &&
                    (runtime.seconds() < timeoutS) &&
                    (hardware.leftDrive.motor1.isBusy() && hardware.rightDrive.motor1.isBusy())) {

//                percentComplete = (((hardware.leftDrive.motor1.getCurrentPosition() - originalLeft)/ (double) distToCounts) + ((hardware.rightDrive.motor1.getCurrentPosition() - originalRight)/ (double) distToCounts))/2;
                percentComplete = MiscFuncs.average(Math.abs(Math.abs(hardware.leftDrive.motor1.getCurrentPosition()) - originalLeftAbs)/(double) absDistToCounts, Math.abs(Math.abs(hardware.rightDrive.motor1.getCurrentPosition()) - originalRightAbs)/(double) absDistToCounts);
                if (percentComplete > 1) {
                    percentComplete -= 1;
                }
                speed = Math.max(0.1,Math.abs(MiscFuncs.oneThirdTwoThirdEasingSine(percentComplete) * maxPower));
                hardware.leftDrive.setPower(speed);
                hardware.rightDrive.setPower(speed);
                // Display it for the driver.
                ln.telemetry.addData("Path1",  "Running to %7d :%7d", newLeftTarget,  newRightTarget);
                ln.telemetry.addData("Path2",  "Running at %7d :%7d",
                        hardware.leftDrive.motor1.getCurrentPosition(),
                        hardware.rightDrive.motor1.getCurrentPosition());
                ln.telemetry.update();
            }

            // Stop all motion;
            hardware.leftDrive.setPower(0);
            hardware.rightDrive.setPower(0);

            hardware.rightDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
            hardware.leftDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

            // Turn off RUN_TO_POSITION
            hardware.leftDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            hardware.rightDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

            //  sleep(250);   // optional pause after each move
        }
    }

    public void encoderDriveFirstThirdAccel(double maxPower, double distance, double timeoutS) {
        int newLeftTarget;
        int newRightTarget;

        int originalLeft;
        int originalRight;
        int originalLeftAbs;
        int originalRightAbs;

        ElapsedTime runtime = new ElapsedTime();

        // Ensure that the opmode is still active
        if (ln.opModeIsActive()) {

            // Determine new target position, and pass to motor controller

            int distToCounts = (int)(distance * COUNTS_PER_INCH);
            int absDistToCounts = Math.abs(distToCounts);

            originalLeft = hardware.leftDrive.motor1.getCurrentPosition();
            originalRight = hardware.rightDrive.motor1.getCurrentPosition();
            originalLeftAbs = Math.abs(originalLeft);
            originalRightAbs = Math.abs(originalRight);
            newLeftTarget = hardware.leftDrive.motor1.getCurrentPosition() + distToCounts;
            newRightTarget = hardware.rightDrive.motor1.getCurrentPosition() + distToCounts;
            hardware.leftDrive.setTargetPosition(newLeftTarget);
            hardware.rightDrive.setTargetPosition(newRightTarget);


            // Turn On RUN_TO_POSITION
            hardware.leftDrive.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            hardware.rightDrive.setMode(DcMotor.RunMode.RUN_TO_POSITION);

            // reset the timeout time and start motion.
            runtime.reset();

            // keep looping while we are still active, and there is time left, and both motors are running.
            // Note: We use (isBusy() && isBusy()) in the loop test, which means that when EITHER motor hits
            // its target position, the motion will stop.  This is "safer" in the event that the robot will
            // always end the motion as soon as possible.
            // However, if you require that BOTH motors have finished their moves before the robot continues
            // onto the next step, use (isBusy() || isBusy()) in the loop test.
            double percentComplete;
            double speed;
            while (ln.opModeIsActive() &&
                    (runtime.seconds() < timeoutS) &&
                    (hardware.leftDrive.motor1.isBusy() && hardware.rightDrive.motor1.isBusy())) {

                percentComplete = MiscFuncs.average(Math.abs(Math.abs(hardware.leftDrive.motor1.getCurrentPosition()) - originalLeftAbs)/(double) absDistToCounts, Math.abs(Math.abs(hardware.rightDrive.motor1.getCurrentPosition()) - originalRightAbs)/(double) absDistToCounts);
                if (percentComplete > 1) {
                    percentComplete -= 1;
                }
                speed = Math.max(0.1,Math.abs(MiscFuncs.oneThirdAccelSine(percentComplete) * maxPower));
                hardware.leftDrive.setPower(speed);
                hardware.rightDrive.setPower(speed);
                // Display it for the driver.
                ln.telemetry.addData("Path1",  "Running to %7d :%7d", newLeftTarget,  newRightTarget);
                ln.telemetry.addData("Path2",  "Running at %7d :%7d",
                        hardware.leftDrive.motor1.getCurrentPosition(),
                        hardware.rightDrive.motor1.getCurrentPosition());
                ln.telemetry.update();
            }

            // Stop all motion;
            hardware.leftDrive.setPower(0);
            hardware.rightDrive.setPower(0);

            hardware.rightDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
            hardware.leftDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

            // Turn off RUN_TO_POSITION
            hardware.leftDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            hardware.rightDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

            //  sleep(250);   // optional pause after each move
        }
    }

    public enum whichSide {
        LEFT,
        RIGHT
    }

    public void moveWhileMaintainingHeading(double power, whichSide side, double timeS) {
        ElapsedTime runtime = new ElapsedTime();

        if (power > 0.5) {
            power = 0.5;
        } else if (power < -0.5) {
            power = -0.5;
        }

        double gain = 0.7;

        imuController.resetRelativeAngle();

        double correctionPower;

        if (side == whichSide.LEFT) {
            hardware.rightDrive.setPower(power);
            hardware.leftDrive.setPower(power);
            while (ln.opModeIsActive() && Math.abs(imuController.getRelativeAngle()) < 5) {
                ln.telemetry.addData("relAng: ", imuController.getRelativeAngle());
                ln.telemetry.update();
                ln.idle();
            }
            runtime.reset();
            while (ln.opModeIsActive() && runtime.seconds() < timeS) {
                correctionPower = -(imuController.getRelativeAngle()/30) * gain;
                hardware.leftDrive.setPower(correctionPower);
                ln.telemetry.addData("relAng: ", imuController.getRelativeAngle());
                ln.telemetry.addData("correctionPower", correctionPower);
                ln.telemetry.update();
            }
        } else if (side == whichSide.RIGHT /* && ln.opModeIsActive() */) {
            hardware.rightDrive.setPower(power);
            hardware.leftDrive.setPower(power);
            while (ln.opModeIsActive() && Math.abs(imuController.getRelativeAngle()) < 5) {
                ln.telemetry.addData("relAng: ", imuController.getRelativeAngle());
                ln.telemetry.update();
                ln.idle();
            }
            runtime.reset();
            while (ln.opModeIsActive() && runtime.seconds() < timeS) {
                correctionPower = (imuController.getRelativeAngle()/30) * gain;
                hardware.rightDrive.setPower(correctionPower);
                ln.telemetry.addData("relAng: ", imuController.getRelativeAngle());
                ln.telemetry.addData("correctionPower", correctionPower);
                ln.telemetry.update();
            }
        }
    }

    @Deprecated
    public void imuTurn(double angle, Direction direction, boolean useGlobalAngle) {
        hardware.rightDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        hardware.leftDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        double deltaAngle;

        if (useGlobalAngle) {
            if (direction == Direction.clockwise && angle < imuController.getGlobalAngle()) {
                deltaAngle = (360 - imuController.getGlobalAngle()) + angle;
            } else if (direction == Direction.counterclockwise && angle > imuController.getGlobalAngle()) {
                deltaAngle = -((360 - angle) + imuController.getGlobalAngle());
            } else {
                deltaAngle = angle - imuController.getGlobalAngle();
            }
        } else {
            deltaAngle = angle;
        }

        imuController.resetRelativeAngle();

        double maxPower = Math.abs(deltaAngle / 60);

        if (maxPower > 1) {
            maxPower = 1;
        }

        double powerPercent;
        double percentCompleteAbs = imuController.getRelativeAngle() / deltaAngle;
        double percentCompleteVectorThing;
        double absDeltaAngle = Math.abs(deltaAngle);
        double relativeAngle;

        while (percentCompleteAbs < 100 || Math.toDegrees(Math.abs((hardware.imu.getAngularVelocity().zRotationRate))) < 5) {
            relativeAngle = imuController.getRelativeAngle();
            percentCompleteAbs = relativeAngle / deltaAngle;

            if ( (deltaAngle < 0 && relativeAngle < deltaAngle) || (deltaAngle > 0 && relativeAngle > deltaAngle) ) {
                percentCompleteVectorThing = (deltaAngle - relativeAngle) / absDeltaAngle;
                ln.telemetry.addLine("0:0");
            } else {
                percentCompleteVectorThing = relativeAngle / absDeltaAngle;
                ln.telemetry.addLine("0:1");
            }

            if (percentCompleteVectorThing < 0) {
                powerPercent = Math.max(0.1, MiscFuncs.oneThirdTwoThirdEasingSine(percentCompleteVectorThing) * maxPower);
                ln.telemetry.addLine("1:0");
            } else {
                powerPercent = Math.min(-0.1, MiscFuncs.oneThirdTwoThirdEasingSine(percentCompleteVectorThing) * maxPower);
                ln.telemetry.addLine("1:1");
            }

            rotate(powerPercent);

            ln.telemetry.addData("angle", relativeAngle);
            ln.telemetry.addData("power: ", powerPercent);
            ln.telemetry.addData("percentComplete: ", percentCompleteAbs);
            ln.telemetry.addData("AngVeloc: ", hardware.imu.getAngularVelocity().zRotationRate);
            ln.telemetry.update();
        }
    }



    @Deprecated
    public void imuTurn(double maxPower, double angle, Direction direction, boolean useGlobalAngle) {
        hardware.rightDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        hardware.leftDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        double deltaAngle = 0;

        if (useGlobalAngle) {
            if (direction == Direction.clockwise && angle < imuController.getGlobalAngle()) {
                deltaAngle = (360 - imuController.getGlobalAngle()) + angle;
            } else if (direction == Direction.counterclockwise && angle > imuController.getGlobalAngle()) {
                deltaAngle = -((360 - angle) + imuController.getGlobalAngle());
            } else {
                deltaAngle = angle - imuController.getGlobalAngle();
            }
        } else {
            deltaAngle = angle;
        }

        imuController.resetRelativeAngle();

        double powerPercent;


        double percentCompleteAbs = imuController.getRelativeAngle() / deltaAngle;
        double percentCompleteVectorThing;
        double absDeltaAngle = Math.abs(deltaAngle);

        while (percentCompleteAbs < 100 || Math.toDegrees(Math.abs((hardware.imu.getAngularVelocity().zRotationRate))) < 5) {
            percentCompleteAbs = imuController.getRelativeAngle() / deltaAngle;

            if ( (deltaAngle < 0 && imuController.getRelativeAngle() < deltaAngle) || (deltaAngle > 0 && imuController.getRelativeAngle() > deltaAngle) ) {
                percentCompleteVectorThing = (deltaAngle - imuController.getRelativeAngle()) / absDeltaAngle;
            } else {
                percentCompleteVectorThing = imuController.getRelativeAngle() / absDeltaAngle;
            }

            if (percentCompleteVectorThing < 0) {
                powerPercent = Math.min(-0.1, MiscFuncs.oneThirdTwoThirdEasingSine(percentCompleteVectorThing) * maxPower);
            } else {
                powerPercent = Math.max(0.1, MiscFuncs.oneThirdTwoThirdEasingSine(percentCompleteVectorThing) * maxPower);
            }

            rotate(powerPercent);

            ln.telemetry.addData("angle", imuController.getRelativeAngle());
            ln.telemetry.addData("power: ", powerPercent);
            ln.telemetry.addData("percentComplete: ", percentCompleteAbs);
            ln.telemetry.addData("AngVeloc: ", hardware.imu.getAngularVelocity().zRotationRate);
            ln.telemetry.update();
        }
    }

    void rotate(double power) {
        hardware.leftDrive.setPower(power);
        hardware.rightDrive.setPower(-power);
    }

    RobotConfig hardware;
    LinearOpMode ln;
    Orientation angles;
    Orientation lastAngles;
    ImuController3 imuController;
    public ChassisController3(RobotConfig robot, LinearOpMode ln, ImuController3 imuController) {
        hardware = robot;
        if (!robot.doThingsMakeSenseForAuton) {
            throw new IllegalStateException("Make things make sense lol");
        }
        this.ln = ln;
//        robot = robot;
        this.imuController = imuController;
//        ln = ln;
    }
    public enum EasingTypes {
        PROPORTIONAL,
        QUADRATIC,
        SINE
    }
    public enum Direction {
        clockwise,
        counterclockwise
    }
}
