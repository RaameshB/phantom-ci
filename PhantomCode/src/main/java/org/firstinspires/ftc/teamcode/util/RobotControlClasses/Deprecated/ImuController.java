package org.firstinspires.ftc.teamcode.util.RobotControlClasses.Deprecated;

import com.qualcomm.hardware.bosch.BNO055IMU;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;

import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;
import org.firstinspires.ftc.teamcode.util.RobotConfig;
import org.firstinspires.ftc.teamcode.util.RobotControlClasses.Deprecated.ChassisController2;

@Deprecated
public class ImuController {
    BNO055IMU imu;
    LinearOpMode linearOpMode;
    RobotConfig robot;
    Orientation angles;
    Orientation lastAngles = new Orientation();
    double lastAngle = 0;
    public double globalAngle = 0;
    public double relativeAngle = 0;
    public double unboundRelativeAngle = 0;
    public ImuController(RobotConfig robot, LinearOpMode linearOpMode) {
        this.robot = robot;
        this.imu = this.robot.imu;
        this.linearOpMode = linearOpMode;
        lastAngles = imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
//        lastAngles = imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
//        angles = imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
    }
    @Deprecated
    ImuController() {
//        lastAngles = imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
    }

    @Deprecated
    private void resetAngle()
    {
        lastAngles = imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);

        globalAngle = 0;
    }

    public void resetRelativeAngle() {
        lastAngles = imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);

        relativeAngle = 0;
    }
    public void resetUnboundRelativeAngle() {
        lastAngles = imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);

        unboundRelativeAngle = 0;
    }

    /**
     * Get current cumulative angle rotation from last reset.
     * @return Angle in degrees. + = left, - = right.
     */
    @Deprecated
    public double getAngle()
    {
        // We experimentally determined the Z axis is the axis we want to use for heading angle.
        // We have to process the angle because the imu works in euler angles so the Z axis is
        // returned as 0 to +180 or 0 to -180 rolling back to -179 or +179 when rotation passes
        // 180 degrees. We detect this transition and track the total cumulative angle of rotation.

        Orientation angles = imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);

        double deltaAngle = angles.firstAngle - lastAngles.firstAngle;

        if (deltaAngle < -180)
            deltaAngle += 360;
        else if (deltaAngle > 180)
            deltaAngle -= 360;

        globalAngle += deltaAngle;

        relativeAngle += deltaAngle;

        lastAngles = angles;

        return globalAngle;
    }

    void updateAngle() {
        Orientation angles = imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);

        double deltaAngle = angles.firstAngle - lastAngles.firstAngle;

        unboundRelativeAngle += deltaAngle;

        if (deltaAngle < -180)
            deltaAngle += 360;
        else if (deltaAngle > 180)
            deltaAngle -= 360;

        globalAngle = angles.firstAngle;

        relativeAngle += deltaAngle;

        lastAngles = angles;
    }


    public double getGlobalAngle() {
        updateAngle();
        return globalAngle;
    }

    public double getRelativeAngle() {
        updateAngle();
        return relativeAngle;
    }

    public double getUnboundRelativeAngle () {
        updateAngle();
        return unboundRelativeAngle;
    }


    /**
     * See if we are moving in a straight line and if not return a power correction value.
     * @return Power adjustment, + is adjust left - is adjust right.
     */
    @Deprecated
    private double checkDirection()
    {
        // The gain value determines how sensitive the correction is to direction changes.
        // You will have to experiment with your robot to get small smooth direction changes
        // to stay on a straight line.
        double correction, angle, gain = .10;

        angle = getAngle();

        if (angle == 0)
            correction = 0;             // no adjustment.
        else
            correction = -angle;        // reverse sign of angle for correction.

        correction = correction * gain;

        return correction;
    }

    /**
     * Rotate left or right the number of degrees. Does not support turning more than 180 degrees.
     * @param degrees Degrees to turn, + is left - is right
     */
    @Deprecated
    public void rotate(int degrees, double power)
    {
        double  leftPower, rightPower;

        // restart imu movement tracking.
        resetAngle();

        // getAngle() returns + when rotating counter clockwise (left) and - when rotating
        // clockwise (right).

        if (degrees < 0)
        {   // turn right.
            /*leftPower = power;
            rightPower = -power;*/
            leftPower = -power;
            rightPower = power;
        }
        else if (degrees > 0)
        {   // turn left.
            /*leftPower = -power;
            rightPower = power;*/
            leftPower = power;
            rightPower = -power;
        }
        else return;

        // set power to rotate.
        robot.leftDrive.setPower(leftPower);
        robot.rightDrive.setPower(rightPower);

        // rotate until turn is completed.
        if (degrees < 0)
        {
            // On right turn we have to get off zero first.
            while (linearOpMode.opModeIsActive() && getAngle() == 0) {}

            while (linearOpMode.opModeIsActive() && getAngle() > degrees) {}
        }
        else    // left turn.
            while (linearOpMode.opModeIsActive() && getAngle() < degrees) {}

        // turn the motors off.
        robot.rightDrive.setPower(0);
        robot.leftDrive.setPower(0);

        // wait for rotation to stop.
        linearOpMode.sleep(1000);

        // reset angle tracking on new heading.
        resetAngle();
    }

//    @Deprecated
//    public void rotateButItActuallyMakesSense(int degrees, double power)
//    {
//        double  leftPower, rightPower;
//
//        // restart imu movement tracking.
//        resetAngle();
//
//        // getAngle() returns + when rotating counter clockwise (left) and - when rotating
//        // clockwise (right).
//
//        if (degrees < 0)
//        {   // turn right.
//            /*leftPower = power;
//            rightPower = -power;*/
//            leftPower = -power;
//            rightPower = power;
//        }
//        else if (degrees > 0)
//        {   // turn left.
//            /*leftPower = -power;
//            rightPower = power;*/
//            leftPower = power;
//            rightPower = -power;
//        }
//        else return;
//
//        // set power to rotate.
//        robot.leftDrive.setPower(leftPower);
//        robot.rightDrive.setPower(rightPower);
//
//        // rotate until turn is completed.
//        if (degrees < 0)
//        {
//            // On right turn we have to get off zero first.
//            while (linearOpMode.opModeIsActive() && getAngle() == 0) {}
//
//            while (linearOpMode.opModeIsActive() && getAngle() > degrees) {}
//        }
//        else    // left turn.
//            while (linearOpMode.opModeIsActive() && getAngle() < degrees) {}
//
//        // turn the motors off.
//        robot.rightDrive.setPower(0);
//        robot.leftDrive.setPower(0);
//
//        // wait for rotation to stop.
//        linearOpMode.sleep(1000);
//
//        // reset angle tracking on new heading.
//        resetAngle();
//    }

    public void imuTurn (double maxPower, double angle, ChassisController2.EasingTypes easing, boolean useGlobalAngle) {
        robot.rightDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        robot.leftDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        double deltaAngle = 0;

        if (useGlobalAngle) {
            if (angle > 360) {
                do {
                    angle -= 360;
                } while (angle > 360);
            } else if (angle < 0) {
                do {
                    angle += 360;
                } while (angle < 0);
            }
            deltaAngle = angle - getAngle();

            if (deltaAngle > 180) {
                deltaAngle -= 360;
            } else if (deltaAngle < -180) {
                deltaAngle += 360;
            }

        } else if (angle > 360) {
            do {
                angle -= 360;
            } while (angle > 360);
            if (angle > 180) {
                angle = 180 - angle;
            }
        } else if (angle < -360) {
            do {
                angle += 360;
            } while (angle < -360);
            if (angle < -180) {
                angle = -180 - angle;
            }

            deltaAngle = angle;
        }

        deltaAngle = 359;

        double midway = deltaAngle / 2;

        resetAngle();

        double powerPercent;

        if (easing == ChassisController2.EasingTypes.PROPORTIONAL) {
            if (deltaAngle > 0) {
                while (getAngle() < midway) {
                    powerPercent = getAngle() / midway;
                    rotate(Math.max(powerPercent  * maxPower, 0.1), ChassisController2.Direction.counterclockwise);
                    linearOpMode.telemetry.addData("angle", getAngle());
                    linearOpMode.telemetry.addData("power: ", powerPercent);
                    linearOpMode.telemetry.update();
                }
                while (getAngle() < deltaAngle) {
                    powerPercent = 1 - ((getAngle() - midway)/midway);
                    rotate(Math.max(powerPercent * maxPower, 0.1), ChassisController2.Direction.counterclockwise);
                    linearOpMode.telemetry.addData("angle", getAngle());
                    linearOpMode.telemetry.addData("power: ", powerPercent);
                    linearOpMode.telemetry.update();
                }
            }
            if (deltaAngle < 0) {
                while (getAngle() > midway) {
                    powerPercent = getAngle() / midway;
                    rotate(Math.max(powerPercent * maxPower, 0.2), ChassisController2.Direction.clockwise);
                    linearOpMode.telemetry.addData("angle", getAngle());
                    linearOpMode.telemetry.addData("power: ", powerPercent);
                    linearOpMode.telemetry.update();
                }
                while (getAngle() >= deltaAngle) {
                    powerPercent = 1 - ((getAngle() - midway)/midway);
                    rotate(Math.max(powerPercent * maxPower, 0.2), ChassisController2.Direction.clockwise);
                    linearOpMode.telemetry.addData("angle", getAngle());
                    linearOpMode.telemetry.addData("power: ", powerPercent);
                    linearOpMode.telemetry.update();
                }
            }
        }
        robot.rightDrive.setPower(0);
        robot.leftDrive.setPower(0);
    }



    void rotate(double power, ChassisController2.Direction direction) {
        if (direction == ChassisController2.Direction.clockwise) {
            robot.leftDrive.setPower(-power);
            robot.rightDrive.setPower(power);
        } else {
            robot.leftDrive.setPower(power);
            robot.rightDrive.setPower(-power);
        }
    }
}