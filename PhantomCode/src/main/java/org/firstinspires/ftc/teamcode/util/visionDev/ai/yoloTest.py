import torch

# model = torch.hub.load('ultralytics/yolov5', 'yolov5s')

model = torch.load(r'/home/raameshb/Documents/GitRepos/PhantomCI/PhantomCode/src/main/java/org/firstinspires/ftc/teamcode/util/vision/ai/yolov5-master/runs/train/exp5/weights/best.pt')

img = r"/home/raameshb/Documents/GitRepos/PhantomCI/PhantomCode/src/main/java/org/firstinspires/ftc/teamcode/util/vision/ai/dataset/JPEGImages/image0.jpg"

results = model(img)

results.print()