package org.firstinspires.ftc.teamcode.util.visionDev.easyopencv.sim;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.openftc.easyopencv.OpenCvPipeline;

public class simPipeline extends OpenCvPipeline {

    Mat mat = new Mat();
    public enum Locations {
        Left,
        Right,
        Other_Slot
    }
    private Locations location;

    static final Rect Left_ROI = new Rect(
            new Point(641 , 107),
            new Point(976 , 387)

    );
    static final Rect Right_ROI = new Rect(
            new Point(80 , 115),
            new Point(369 , 320)
    );
    static double PERCENT_COLOR_THRESHOLD = 0.4;


    Mat newMat = new Mat();

    @Override
    public Mat processFrame(Mat input){
        Imgproc.cvtColor(input, mat, Imgproc.COLOR_RGB2HSV);
        Scalar lowYellow = new Scalar(20,80,130);
        Scalar upperYellow = new Scalar(60,255,255);

        Core.inRange(mat, lowYellow, upperYellow, mat);
        Mat right = mat.submat(Right_ROI);
        Mat left = mat.submat(Left_ROI);

        double leftValue = Core.sumElems(left).val[0] / Left_ROI.area() / 255;
        double rightValue = Core.sumElems(left).val[0] / Right_ROI.area() / 255;


        left.release();
        right.release();

        boolean duckLEFT = leftValue > PERCENT_COLOR_THRESHOLD;
        boolean duckRIGHT = rightValue > PERCENT_COLOR_THRESHOLD;

        if (duckLEFT && duckRIGHT){
            location = Locations.Other_Slot;
//            telemetry.addData("Duck Location" , "Other Slot");
        } else {
            if (duckLEFT) {
                location = Locations.Right;
//                telemetry.addData("Duck Location" , "Right");
            } else {
                location = Locations.Left;
//                telemetry.addData("Duck Location" , "Left");
            }
        }
//        telemetry.update();

        Imgproc.cvtColor(mat , mat , Imgproc.COLOR_HSV2RGB);

        Scalar colorDUCK = new Scalar(0, 255, 0);
        Scalar colorNonDuck = new Scalar(255, 0, 0);

        Imgproc.rectangle(mat, Left_ROI, location == Locations.Left? colorDUCK:colorNonDuck);
        Imgproc.rectangle(mat, Right_ROI, location == Locations.Right? colorDUCK:colorNonDuck);

        newMat = mat;

        return mat;

    }
}
