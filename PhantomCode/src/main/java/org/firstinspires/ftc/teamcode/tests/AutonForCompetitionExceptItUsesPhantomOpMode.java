package org.firstinspires.ftc.teamcode.tests;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.hardware.DcMotor;

import org.firstinspires.ftc.teamcode.util.PhantomOpMode2;
import org.firstinspires.ftc.teamcode.util.RobotControlClasses.Deprecated.ChassisController2;
import org.firstinspires.ftc.teamcode.util.enums.Positions;

@Disabled
@Autonomous
public class AutonForCompetitionExceptItUsesPhantomOpMode extends PhantomOpMode2 {

    Positions pos;

    @Override
    public void opModeCode() throws InterruptedException {
        waitForStart();

        robotControl.cvWebcamUtils.startAsyncStream();

        hardware.slider.setTargetPosition(-125);
        hardware.slider.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        hardware.slider.setPower(0.8);

        while (robotControl.cvWebcamUtils.getPos() == Positions.stillProc) {
            idle();
        }
        sleep(1800);
        pos = robotControl.cvWebcamUtils.getPos();
        robotControl.cvWebcamUtils.stopStream();

        switch (pos) {
            case leftBot:
                hardware.slider.setTargetPosition(-287);
                break;
            case  midMid:
                hardware.slider.setTargetPosition(-640);
                break;
            case rightTop:
                hardware.slider.setTargetPosition(-807);
        }

        robotControl.driveUtils.encoderDrive(12, 2);

        hardware.bucket.setPosition(0.5);

        robotControl.driveUtils.imuTurn(330, ChassisController2.Direction.counterclockwise, true);
        robotControl.driveUtils.encoderDrive(-13, 5);
        robotControl.driveUtils.moveWhileMaintainingHeading(0.2, ChassisController2.whichSide.RIGHT, 0.7);

        hardware.carousel.setPower(-1.0);
        sleep(2750);
        hardware.carousel.setPower(0);

        robotControl.driveUtils.encoderDrive(37.75, 5);
        robotControl.driveUtils.imuTurn(325, ChassisController2.Direction.counterclockwise, true);
        robotControl.driveUtils.encoderDrive(2.5, 1);

        sleep(2000);
        hardware.bucket.setPosition(1.0);
        sleep(1000);
        hardware.bucket.setPosition(0);

        hardware.slider.setTargetPosition(0);

        robotControl.driveUtils.encoderDrive(-20, 5);
        robotControl.driveUtils.imuTurn(309, ChassisController2.Direction.counterclockwise, true);
        robotControl.driveUtils.encoderDriveFirstThirdAccel(1.0, 90, 10);
    }
}

