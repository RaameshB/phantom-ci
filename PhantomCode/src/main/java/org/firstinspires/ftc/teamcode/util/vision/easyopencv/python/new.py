
import cv2
import matplotlib.pyplot as plt

# video = cv2.VideoCapture(0)

original = cv2.imread(r"tse/none.jpg")
original = cv2.resize(original, [320, 176])
submatRight = original[42:153, 76:200]
submatLeft = original[42:153, 0:60]

# plt.imshow(submat)
# plt.show()

labr = cv2.cvtColor(submatRight, cv2.COLOR_BGR2Lab)
labl = cv2.cvtColor(submatLeft, cv2.COLOR_BGR2Lab)

# plt.imshow(crcb)
# plt.show()

# plt.imshow(labr)
# plt.show()
# plt.imshow(labl)
# plt.show()

# print(ycrcb)

lr = cv2.extractChannel(labr, 0)
ar = cv2.extractChannel(labr, 1)
br = cv2.extractChannel(labr, 2)

# ar = cv2.equalizeHist(ar)
# br = cv2.equalizeHist(br)

# plt.imshow(cv2.cvtColor(ar, cv2.COLOR_GRAY2RGB))
# plt.show()
plt.imshow(cv2.cvtColor(br, cv2.COLOR_GRAY2RGB))
plt.show()

ll = cv2.extractChannel(labl, 0)
al = cv2.extractChannel(labl, 1)
bl = cv2.extractChannel(labl, 2)

# al = cv2.equalizeHist(al)
# bl = cv2.equalizeHist(bl)

# plt.imshow(cv2.cvtColor(al, cv2.COLOR_GRAY2RGB))
# plt.show()
plt.imshow(cv2.cvtColor(bl, cv2.COLOR_GRAY2RGB))
plt.show()

# print(cv2.mean(ar)[0])
# print(cv2.mean(br)[0])

print()

avgr = (cv2.mean(ar)[0]+cv2.mean(br)[0])/2
print(avgr)

print()

# print(cv2.mean(al)[0])
# print(cv2.mean(bl)[0])

avgl = (cv2.mean(al)[0]+cv2.mean(bl)[0])/2 * 1.015628337
print(avgl)



if (avgr > avgl + 2.5):
    plt.imshow(cv2.cvtColor(cv2.rectangle(original,(76, 42), (200, 153), (255, 0, 0)), cv2.COLOR_BGR2RGB))
    plt.show()
elif (avgl > avgr + 2.5):
    plt.imshow(cv2.cvtColor(cv2.rectangle(original,(0, 42), (60, 153), (255, 0, 0)), cv2.COLOR_BGR2RGB))
    plt.show()
else:
    plt.imshow(cv2.cvtColor(original, cv2.COLOR_BGR2RGB))
    plt.show()

