package org.firstinspires.ftc.teamcode.util.visionDev.easyopencv.oldJavaPipelines.Deprecated;

import org.firstinspires.ftc.teamcode.util.enums.Positions;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.openftc.easyopencv.OpenCvPipeline;

public class PipelineMkThree extends OpenCvPipeline {

    Positions pos = Positions.stillProc;

    static final double PERCENT_COLOUR_THRESHOLD = 0.23;

    class Left_ROI {
        public final int x1 = 80;
        public final int y1 = 115;
        public final int x2 = 369;
        public final int y2 = 320;
        public Point startPoint = new Point(x1, y1);
        public Point endPoint = new Point(x2, y2);
    }
    Left_ROI leftRoi = new Left_ROI();
    class Mid_ROI {
        public final int x1 = 641;
        public final int y1 = 107;
        public final int x2 = 976;
        public final int y2 = 387;
        public Point startPoint = new Point(x1, y1);
        public Point endPoint = new Point(x2, y2);
    }
    Mid_ROI midRoi = new Mid_ROI();

    Scalar lowerYellow = new Scalar(20, 80, 130);
    Scalar upperYellow = new Scalar(60, 255, 255);

    Scalar colorBlue = new Scalar(255, 0, 0);
    Scalar colorRed = new Scalar(0, 0 , 255);
    Scalar colorGreen = new Scalar(0, 255, 0);

    int thickness = 2;

//    // TODO: Figure out why the heck this works
//    final double aCompletelyArbitraryValueThatMayFixThings = 2.7717391304347825785680529300567111025211638037314010595525673507456406570373114049386885104640064680577335819129731732855045444242046382010375606064713239017656455818334358503734175887670016263758958;

    public int rectArea(int x1, int x2, int y1, int y2) {
        return (x2 - x1) * (y2 - y1);
    }

    double leftValue = 0;// = Core.sumElems(leftSubmat).val[0] / rectArea(leftRoi.x1, leftRoi.x2, leftRoi.y1, leftRoi.y2) / 255 * aCompletelyArbitraryValueThatMayFixThings;
    double midValue = 0;

    @Override
    public Mat processFrame(Mat input) {
        Imgproc.cvtColor(input, input, Imgproc.COLOR_BGR2HSV);

        Mat inRangeMat = new Mat();
        Core.inRange(input, lowerYellow, upperYellow, inRangeMat);

        Mat leftSubmat = inRangeMat.submat(leftRoi.y1, leftRoi.y2, leftRoi.x1, leftRoi.x2);
        Mat midSubmat = inRangeMat.submat(midRoi.y1, midRoi.y2, midRoi.x1, midRoi.x2);

        leftValue = Core.sumElems(leftSubmat).val[0] / rectArea(leftRoi.x1, leftRoi.x2, leftRoi.y1, leftRoi.y2) / 255/* * aCompletelyArbitraryValueThatMayFixThings*/;
        midValue = Core.sumElems(midSubmat).val[0] / rectArea(midRoi.x1, midRoi.x2, midRoi.y1, midRoi.y2) / 255/* * aCompletelyArbitraryValueThatMayFixThings*/;

        inRangeMat.release();
        leftSubmat.release();
        midSubmat.release();

        if (leftValue > PERCENT_COLOUR_THRESHOLD) {
            pos = Positions.rightTop;
        } else if (midValue > PERCENT_COLOUR_THRESHOLD) {
            pos = Positions.midMid;
        } else {
            pos = Positions.leftBot;
        }

        Imgproc.cvtColor(input, input, Imgproc.COLOR_HSV2BGR);
        if (pos == Positions.rightTop) {
            Imgproc.rectangle(input, leftRoi.startPoint, leftRoi.endPoint, colorRed, thickness);
            Imgproc.rectangle(input, midRoi.startPoint, midRoi.endPoint, colorBlue, thickness);
        } else if (pos == Positions.midMid) {
            Imgproc.rectangle(input, leftRoi.startPoint, leftRoi.endPoint, colorBlue, thickness);
            Imgproc.rectangle(input, midRoi.startPoint, midRoi.endPoint, colorRed, thickness);
        } else {
            Imgproc.rectangle(input, leftRoi.startPoint, leftRoi.endPoint, colorGreen, thickness);
            Imgproc.rectangle(input, midRoi.startPoint, midRoi.endPoint, colorGreen, thickness);
        }

        return input;
    }

    public Positions getPos() {
        return pos;
    }
    public double getLeftValue() {
        return leftValue;
    }
    public double getMidValue() {
        return  midValue;
    }
}
