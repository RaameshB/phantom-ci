package org.firstinspires.ftc.teamcode.tests.why_did_you_open_this.stop.no;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.teamcode.util.RobotConfig;
import org.firstinspires.ftc.teamcode.util.RobotControlClasses.Deprecated.ImuController;

public class oldLibShowAuto extends LinearOpMode {

    RobotConfig robot;
    ImuController imuController;

    @Override
    public void runOpMode() throws InterruptedException {
        robot = new RobotConfig();
        robot.init(hardwareMap, this);
        imuController = new ImuController(robot, this);
        waitForStart();
        imuController.resetRelativeAngle();
        imuController.rotate(90, 0.3);
//        double  imuController.getRelativeAngle();

    }
}
