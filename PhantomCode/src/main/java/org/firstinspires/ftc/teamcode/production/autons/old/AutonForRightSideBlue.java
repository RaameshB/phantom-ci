package org.firstinspires.ftc.teamcode.production.autons.old;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.util.RobotConfig;
import org.firstinspires.ftc.teamcode.util.RobotControlClasses.CvWebcamController;
import org.firstinspires.ftc.teamcode.util.RobotControlClasses.Deprecated.ImuController;
import org.firstinspires.ftc.teamcode.util.RobotControlClasses.Deprecated.SliderController;
import org.firstinspires.ftc.teamcode.util.enums.Positions;
@Disabled
@Autonomous(group = "TestAutos")
public class AutonForRightSideBlue extends LinearOpMode {
    RobotConfig robot = new RobotConfig();

    static final double COUNTS_PER_REVOLUTION = 1120;
    static final double SPROCKET_REDUCTION = 1.5;
    static final double GEAR_REDUCTION = 0.5;
    static final double WHEEL_DIAMETER_INCHES = 4;
    static final double COUNTS_PER_INCH = (COUNTS_PER_REVOLUTION * SPROCKET_REDUCTION * GEAR_REDUCTION) / (WHEEL_DIAMETER_INCHES * Math.PI);

    private ElapsedTime runtime = new ElapsedTime();
    Positions pos;

    // 0, -287, -669
    @Override
    public void runOpMode() throws InterruptedException {

        robot.init(hardwareMap, this);
        robot.rightDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        robot.leftDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        robot.rightDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        robot.leftDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        ImuController imuController = new ImuController(robot,this);
        SliderController sliderController = new SliderController(robot, this);
        CvWebcamController cvWebcam = new CvWebcamController(robot);
//        CvWebcamController cvWebcam = new CvWebcamController(robot);
        waitForStart();
//        cvWebcam.startAsyncStream();
//        robot.slider.setTargetPosition(-125);
//        robot.slider.setMode(DcMotor.RunMode.RUN_TO_POSITION);
//        robot.slider.setPower(0.8);
//        while (cvWebcam.getPos() == Positions.stillProc) {
//            idle();
//        }
//        sleep(1800);
//        pos = cvWebcam.getPos();
//        cvWebcam.stopStream();
//        telemetry.addData("pos: ", pos);
//        telemetry.update();
//        sleep(1000);
        cvWebcam.startAsyncStream();
        robot.slider.setTargetPosition(-125);
        robot.bucket.setPosition(0.5);
        robot.slider.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        robot.slider.setPower(0.8);
        while (cvWebcam.getPos() == Positions.stillProc) {
            idle();
        }
        sleep(1800);
        pos = cvWebcam.getPos();
        cvWebcam.stopStream();
        switch (pos) {
            case leftBot:
                robot.slider.setTargetPosition(-287);
                break;
            case midMid:
                robot.slider.setTargetPosition(-640);
                break;
            case rightTop:
                robot.slider.setTargetPosition(-807);
        }
        //imuController.rotate(-30, 0.5);
//        switch (pos) {
//            case rightBot:
//
//                robot.slider.setTargetPosition(-287);
//                break;
//            case midMid:
//                robot.slider.setTargetPosition(-669);
//                break;
//            case leftTop:
//                robot.slider.setTargetPosition(-807);
//        }
        telemetry.addData("pos",pos);
        encoderDrive(0.5 , 4,4, 5);
        imuController.rotate(-14 , 0.5);
        encoderDrive(.6,21.5,21.5, 4);
        robot.bucket.setPosition(1.0);
        sleep(1000);
        robot.bucket.setPosition(0);
        robot.slider.setTargetPosition(0);
        encoderDrive(0.5 , -12,-12,5);
        imuController.rotate(-40,0.5);
        encoderDrive(0.8, -43,-43,5);
    }

    public void encoderDrive(double speed,
                             double leftInches, double rightInches,
                             double timeoutS) {
        int newLeftTarget;
        int newRightTarget;
//hi
        // Ensure that the opmode is still active
        if (opModeIsActive()) {

            // Determine new target position, and pass to motor controller
            newLeftTarget = robot.leftDrive.motor1.getCurrentPosition() + (int)(leftInches * COUNTS_PER_INCH);
            newRightTarget = robot.rightDrive.motor1.getCurrentPosition() + (int)(rightInches * COUNTS_PER_INCH);
            robot.leftDrive.setTargetPosition(newLeftTarget);
            robot.rightDrive.setTargetPosition(newRightTarget);


            // Turn On RUN_TO_POSITION
            robot.leftDrive.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            robot.rightDrive.setMode(DcMotor.RunMode.RUN_TO_POSITION);

            // reset the timeout time and start motion.
            runtime.reset();
            robot.leftDrive.setPower(Math.abs(speed));
            robot.rightDrive.setPower(Math.abs(speed));

            // keep looping while we are still active, and there is time left, and both motors are running.
            // Note: We use (isBusy() && isBusy()) in the loop test, which means that when EITHER motor hits
            // its target position, the motion will stop.  This is "safer" in the event that the robot will
            // always end the motion as soon as possible.
            // However, if you require that BOTH motors have finished their moves before the robot continues
            // onto the next step, use (isBusy() || isBusy()) in the loop test.
            while (opModeIsActive() &&
                    (runtime.seconds() < timeoutS) &&
                    (robot.leftDrive.motor1.isBusy() && robot.rightDrive.motor1.isBusy())) {

                // Display it for the driver.
                telemetry.addData("Path1",  "Running to %7d :%7d", newLeftTarget,  newRightTarget);
                telemetry.addData("Path2",  "Running at %7d :%7d",
                        robot.leftDrive.motor1.getCurrentPosition(),
                        robot.rightDrive.motor1.getCurrentPosition());
                telemetry.update();
            }

            // Stop all motion;
            robot.leftDrive.setPower(0);
            robot.rightDrive.setPower(0);

            robot.rightDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
            robot.leftDrive.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

            // Turn off RUN_TO_POSITION
            robot.leftDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            robot.rightDrive.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

            //  sleep(250);   // optional pause after each move
        }
    }
}
