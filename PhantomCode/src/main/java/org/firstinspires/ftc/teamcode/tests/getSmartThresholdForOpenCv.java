package org.firstinspires.ftc.teamcode.tests;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.util.RobotConfig;
import org.firstinspires.ftc.teamcode.util.RobotControlClasses.CvWebcamController2Red;
import org.firstinspires.ftc.teamcode.util.enums.Positions;

@Disabled
@TeleOp
public class getSmartThresholdForOpenCv extends LinearOpMode {

    RobotConfig robot = new RobotConfig();
    CvWebcamController2Red cvWebcam;

    @Override
    public void runOpMode() throws InterruptedException {
        robot.init(hardwareMap, this);
        cvWebcam = new CvWebcamController2Red(robot);
        while (cvWebcam.getPos() != Positions.stillProc  && !isStopRequested()) {
            idle();
        }
        waitForStart();
        sleep(500);
//        cvWebcam.calibrate();
        while(!isStopRequested()) {
//            telemetry.addData("smartThreshold", ) {

        }
    }
}
