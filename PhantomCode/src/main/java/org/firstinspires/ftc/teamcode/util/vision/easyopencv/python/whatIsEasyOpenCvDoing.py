import cv2
import matplotlib.pyplot as plt

original = cv2.imread(r"tse/none.jpg")
rgb = cv2.cvtColor(original, cv2.COLOR_BGR2RGB)
incorrectLab = cv2.cvtColor(rgb, cv2.COLOR_BGR2LAB)
bChannel = cv2.extractChannel(incorrectLab, 2)
showMat = cv2.cvtColor(bChannel, cv2.COLOR_GRAY2RGB)
plt.imshow(showMat)
plt.show()