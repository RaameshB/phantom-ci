package org.firstinspires.ftc.teamcode.production.autons.blue;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.util.RobotConfig;
import org.firstinspires.ftc.teamcode.util.RobotControlClasses.CVC3;
import org.firstinspires.ftc.teamcode.util.RobotControlClasses.CVC3Offset;
import org.firstinspires.ftc.teamcode.util.RobotControlClasses.ChassisController3;
import org.firstinspires.ftc.teamcode.util.RobotControlClasses.ImuController3;
import org.firstinspires.ftc.teamcode.util.enums.LeftAndRightEnum;

@Autonomous(group = "blueForComp", preselectTeleOp = "BlueTeleOp")
public class BlueWarehouse extends LinearOpMode {

    static final double COUNTS_PER_REVOLUTION = 1120;
    static final double SPROCKET_REDUCTION = 1.5;
    static final double GEAR_REDUCTION = 0.5;
    static final double WHEEL_DIAMETER_INCHES = 4;
    static final double COUNTS_PER_INCH = (COUNTS_PER_REVOLUTION * SPROCKET_REDUCTION * GEAR_REDUCTION) / (WHEEL_DIAMETER_INCHES * Math.PI);

    private ElapsedTime runtime = new ElapsedTime();
    LeftAndRightEnum pos;
    volatile RobotConfig robot;
    ImuController3 imuController;
    ChassisController3 chassis;
    CVC3 cvWebcam;

    volatile boolean goDown = false;

    Thread sliderThread = new Thread(new SliderThread());

    // 0, -287, -669
    @Override
    public void runOpMode() throws InterruptedException {
        robot = new RobotConfig();

        robot.initButItWasBetter(hardwareMap, this);

        imuController = new ImuController3(robot, this);
        chassis = new ChassisController3(robot, this, imuController);
        cvWebcam = new CVC3(robot, this);
        cvWebcam.startAsyncStream();

        telemetry.addLine("Opening Camera...");
        telemetry.update();

        imuController.resetRelativeAngle();

        while (cvWebcam.getPos() == LeftAndRightEnum.STILL_PROC && !isStopRequested()) {
            idle();
        }
        sleep(500);
        while (!isStarted() && !isStopRequested()) {
            telemetry.addLine("Waiting For Start...");
            telemetry.addData("Pos: ", cvWebcam.getPos());
            telemetry.update();
            idle();
        }
        waitForStart();
        pos = cvWebcam.getPos();
        cvWebcam.stopStream();
        sliderThread.start();

        telemetry.addData("pos",pos);
        chassis.encoderDrive(4,5);
        robot.bucket.setPosition(0.3);
        chassis.pointUsingImuGlobalAngle(30, ChassisController3.Direction.clockwise, 1.0);
        chassis.encoderDrive(21.5,4);
        robot.bucket.setPosition(1.0);
        sleep(1000);
        goDown = true;
        chassis.encoderDrive(-10,4);
        chassis.pointUsingImuGlobalAngle(85,ChassisController3.Direction.clockwise, 2.5);
        chassis.encoderDriveFirstThirdAccel(1.0,-37,4);

    }

    class SliderThread implements Runnable {
        @Override
        public void run() {
            switch (pos) {
                case NOT:
                    robot.slider.setTargetPosition(-950);
//                    bot = true;
                    break;
                case  LEFT:
                    robot.slider.setTargetPosition(-2400);
                    break;
                case RIGHT:
                    robot.slider.setTargetPosition(-3900);
            }
            robot.slider.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            robot.slider.setPower(1.0);
            while (robot.slider.getCurrentPosition() > -400) {

            }
            robot.bucket.setPosition(0.3);
            while (!goDown) {

            }
            if (robot.slider.getCurrentPosition() > -1500) {
                robot.bucket.setPosition(0);
                ElapsedTime runtimeThread = new ElapsedTime();
                runtimeThread.reset();
                while(runtimeThread.seconds() < 1.5) {

                }
            }
            robot.slider.setPower(0.75);
            robot.slider.setTargetPosition(0);
            while (robot.slider.getCurrentPosition() < -1500) {

            }
            robot.bucket.setPosition(0);
        }
    }
}
