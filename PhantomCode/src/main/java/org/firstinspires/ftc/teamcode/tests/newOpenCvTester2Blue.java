package org.firstinspires.ftc.teamcode.tests;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.robotcore.external.navigation.Orientation;
import org.firstinspires.ftc.teamcode.util.RobotConfig;
import org.firstinspires.ftc.teamcode.util.RobotControlClasses.CvWebcamController2Blue;
import org.firstinspires.ftc.teamcode.util.RobotControlClasses.CvWebcamController2Red;

@Disabled
@TeleOp
public class newOpenCvTester2Blue extends LinearOpMode {
    RobotConfig robot;

    Orientation lastAngles;

    /**
     * From 0 to 360
     */
    private double globalAngle = 0;
    /**
     * From -180 to 180
     */
    private double relativeAngle = 0;

//    ImuController2 imuController;

    CvWebcamController2Blue cvWebcam;
    @Override
    public void runOpMode() throws InterruptedException {
        robot = new RobotConfig();
        robot.initButItWasBetter(hardwareMap, this);
        cvWebcam = new CvWebcamController2Blue(robot);
        cvWebcam.startAsyncStream();
        waitForStart();
        while(opModeIsActive()) {
            telemetry.addData("Postion: ", cvWebcam.getPos());
            telemetry.addData("mid: ", cvWebcam.getMidValue());
            telemetry.addData("right : ", cvWebcam.getRightValue());
            telemetry.update();
            idle();
        }
    }
}
