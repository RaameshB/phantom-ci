package org.firstinspires.ftc.teamcode.production.telops;
 
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.Servo;

import org.firstinspires.ftc.teamcode.util.RobotConfig;


@TeleOp(name = "RedTeleOp", group = "redForComp")
public class RedTeleOp extends LinearOpMode {

    RobotConfig robot = new RobotConfig();

    double leftDrivePower;
    double rightDrivePower;
    double tempPower;
    double carouselSpinnerPower;
    double intakePower;
    double tmp;
    double bucketPos = 0;

    boolean reverse = true;

//    DcMotor carousel;
//    DcMotor intake;

    //    Servo basketDump;
    float powerMult = 10;

    boolean isBumpersPressed = false;
    boolean isIntakeUp = true;
    boolean isTwoLeftBumpPressed = false;
    boolean bucketOverride = false;
    boolean isOneAPressed = false;
    boolean isSliderZeroing = false;
    boolean sliderOverride = false;

    @Override
    public void runOpMode() throws InterruptedException {
        robot.init(hardwareMap, this);

        waitForStart();

        while (!isStopRequested()) {

            // Define Power For the Chassis By Using the Sticks on Gamepad 1
            leftDrivePower = gamepad1.left_stick_y;
            rightDrivePower = gamepad1.right_stick_y;

            // A Power multiplier for more fine control
            if (!gamepad1.right_bumper && !gamepad1.left_bumper) {
                isBumpersPressed = false;
            }
            if (gamepad1.right_bumper && !isBumpersPressed && powerMult < 10) {
                powerMult += 1;
                isBumpersPressed = true;
            }
            if (gamepad1.left_bumper && !isBumpersPressed && powerMult > 1) {
                powerMult -= 1;
                isBumpersPressed = true;
            }
            leftDrivePower *= powerMult / 10;
            rightDrivePower *= powerMult / 10;
            telemetry.addData("Power: ", String.valueOf(powerMult * 10) + "%");

            if (!isOneAPressed) {
                if (gamepad1.a) {
                    reverse = !reverse;
                    isOneAPressed = true;
                }
            } else {
                if (!gamepad1.a) {
                    isOneAPressed = false;
                }
            }

            telemetry.addData("reverse: ", reverse);

            if (reverse) {
                tmp = leftDrivePower;
                leftDrivePower = -rightDrivePower;
                rightDrivePower = -tmp;
            }

            // Set the Power to the motors
            robot.leftDrive.setPower(leftDrivePower);
            robot.rightDrive.setPower(rightDrivePower);


            // Carousel Code
            if (gamepad1.right_trigger > 0.5) {
                robot.carousel.setPower(- 0.6 - (gamepad1.left_trigger * 0.4));
            } else {
                robot.carousel.setPower(0);
            }

            // Intake Spinner Servo Code
            if (gamepad2.dpad_up) {
                intakePower = 1;
//                robot.bucket.setPosition(0);
//                bucketOverride = true;
            } else if (gamepad2.dpad_down) {
                intakePower = -1;
//                robot.bucket.setPosition(0);
//                bucketOverride = true;
            } else {
                intakePower = 0;
//                bucketOverride = false;
            }
            // Intake Lift Code
            try {
                if (!isTwoLeftBumpPressed && gamepad2.left_bumper) {
                    if (isIntakeUp) {
                        robot.intakeRotationServo.setPosition(0.0);
                    } else {
                        robot.intakeRotationServo.setPosition(0.7);
                        intakePower = -intakePower;
                    }
                    isIntakeUp = !isIntakeUp;
                    isTwoLeftBumpPressed = true;
                } else if (!gamepad2.left_bumper) {
                    isTwoLeftBumpPressed = false;
                }
            } catch (Exception e) {

            }

            try {
                robot.rightIntakeSpinnerMotor.setPower(intakePower);
                robot.leftIntakeSpinnerMotor.setPower(intakePower);
            } catch (Exception e) {

            }
            // Slider Code
//            if (gamepad2.a && robot.slider.getCurrentPosition() > -807) {
//                robot.slider.setPower(0.45);
//            } else if (gamepad2.y && robot.slider.getCurrentPosition() < -1) {
//                robot.slider.setPower(-0.45);
            /*            } else */
            if (gamepad2.a) {
//                sliderOverride = true;
            }
            if (!sliderOverride) {
                if ((robot.slider.getCurrentPosition() < 1 && gamepad2.right_stick_y > 0.1) || (robot.slider.getCurrentPosition() > -4600 && gamepad2.right_stick_y < -0.1)) {
                    if (gamepad2.right_stick_y > 0) {
                        robot.slider.setPower(gamepad2.right_stick_y * 1.00);
                    } else {
                        robot.slider.setPower(gamepad2.right_stick_y * 0.75);
                    }
                } else {
                    robot.slider.setPower(0);
                }
            } else {
                robot.slider.setPower(gamepad2.left_stick_y);
            }

            if (robot.slider.getCurrentPosition() < -400 && gamepad2.right_stick_y < 0) {
                bucketPos = 0.3;
            }
            if (robot.slider.getCurrentPosition() > -1300 && gamepad2.right_stick_y > 0) {
                bucketPos = 0;
            }
            if (robot.slider.getCurrentPosition() < -1300) {
                bucketPos = 0.3;
            }
            if (robot.slider.getCurrentPosition() < -400 && gamepad2.right_bumper) {
                bucketPos = 1.0;
            }
            robot.bucket.setPosition(bucketPos);

//            telemetry.addData("Slider Pos: ", robot.slider.getCurrentPosition());
//            telemetry.addData("Override: ", sliderOverride);
//                telemetry.addData("isSliderZeroing: ", isSliderZeroing);
            telemetry.update();
            sleep(10);
        }
    }
}