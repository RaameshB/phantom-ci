package org.firstinspires.ftc.teamcode.production.autons.old;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.util.RobotConfig;
import org.firstinspires.ftc.teamcode.util.RobotControlClasses.ChassisController3;
import org.firstinspires.ftc.teamcode.util.RobotControlClasses.CvWebcamController;
import org.firstinspires.ftc.teamcode.util.RobotControlClasses.ImuController3;
import org.firstinspires.ftc.teamcode.util.enums.Positions;

@Disabled
@Autonomous(group = "final")
public class AutonForRightSide extends LinearOpMode {
//  deprecated kekw

    static final double COUNTS_PER_REVOLUTION = 1120;
    static final double SPROCKET_REDUCTION = 1.5;
    static final double GEAR_REDUCTION = 0.5;
    static final double WHEEL_DIAMETER_INCHES = 4;
    static final double COUNTS_PER_INCH = (COUNTS_PER_REVOLUTION * SPROCKET_REDUCTION * GEAR_REDUCTION) / (WHEEL_DIAMETER_INCHES * Math.PI);

    private ElapsedTime runtime = new ElapsedTime();
    Positions pos;
    RobotConfig robot;
    ImuController3 imuController;
    ChassisController3 chassis;

    // 0, -287, -669
    @Override
    public void runOpMode() throws InterruptedException {
        robot = new RobotConfig();

        robot.initButItWasBetter(hardwareMap, this);

//        telemetry.addLine("Init Complete");

//        sleep(2000);

        imuController = new ImuController3(robot, this);
        chassis = new ChassisController3(robot, this, imuController);
        CvWebcamController cvWebcam = new CvWebcamController(robot);
//        CvWebcamController cvWebcam = new CvWebcamController(robot);
        telemetry.addLine("Opening Camera...");
        telemetry.update();


        waitForStart();
        cvWebcam.startAsyncStream();
        robot.slider.setTargetPosition(-125);
        robot.bucket.setPosition(0.5);
        robot.slider.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        robot.slider.setPower(0.8);
        while (cvWebcam.getPos() == Positions.stillProc) {
            idle();
        }
        sleep(1800);
        pos = cvWebcam.getPos();
        cvWebcam.stopStream();
        switch (pos) {
            case leftBot:
                robot.slider.setTargetPosition(-287);
                break;
            case midMid:
                robot.slider.setTargetPosition(-640);
                break;
            case rightTop:
                robot.slider.setTargetPosition(-807);
        }
        telemetry.addData("pos",pos);

        robot.slider.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        robot.slider.setPower(0.8);
        chassis.encoderDrive(4,5);
        chassis.imuTurnFromLastSeason(20,1.0);
        chassis.encoderDrive(20,4);
        sleep(1000);
        robot.bucket.setPosition(1.0);
        sleep(1000);
        robot.bucket.setPosition(0);
        robot.slider.setTargetPosition(0);
        chassis.encoderDrive(-10,4);
        chassis.pointUsingImuGlobalAngle(90,ChassisController3.Direction.clockwise, 2.5);
        chassis.encoderDrive(-37,4);

    }


}
