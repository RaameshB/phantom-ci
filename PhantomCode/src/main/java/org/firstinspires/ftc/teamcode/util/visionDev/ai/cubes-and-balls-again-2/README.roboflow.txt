
cubes and balls again - v2 this one is actually compatible from the thing from github
==============================

This dataset was exported via roboflow.ai on February 28, 2022 at 4:12 AM GMT

It includes 62 images.
Balls-and-cubes are annotated in YOLO v5 PyTorch format.

The following pre-processing was applied to each image:
* Auto-orientation of pixel data (with EXIF-orientation stripping)
* Resize to 640x640 (Stretch)

No image augmentation techniques were applied.


