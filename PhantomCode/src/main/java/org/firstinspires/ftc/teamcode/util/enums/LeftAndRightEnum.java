package org.firstinspires.ftc.teamcode.util.enums;

public enum LeftAndRightEnum {
    LEFT,
    RIGHT,
    NOT,
    STILL_PROC
}
