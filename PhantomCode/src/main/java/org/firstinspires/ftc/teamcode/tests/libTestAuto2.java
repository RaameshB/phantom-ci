package org.firstinspires.ftc.teamcode.tests;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.robotcore.external.navigation.Orientation;
import org.firstinspires.ftc.teamcode.util.RobotConfig;
import org.firstinspires.ftc.teamcode.util.RobotControlClasses.ChassisController3;
import org.firstinspires.ftc.teamcode.util.RobotControlClasses.ImuController3;

@Disabled
@Autonomous
public class libTestAuto2 extends LinearOpMode {
    RobotConfig robot;

    Orientation lastAngles;

    /**
     * From 0 to 360
     */
    private double globalAngle = 0;
    /**
     * From -180 to 180
     */
    private double relativeAngle = 0;

//    ImuController2 imuController;

    ImuController3 im;
    @Override
    public void runOpMode() throws InterruptedException {
        robot = new RobotConfig();
        robot.initButItWasBetter(hardwareMap, this);
        im = new ImuController3(robot, this);
        im.resetRelativeAngle();
        ChassisController3 chassis = new ChassisController3(robot, this, im);
        waitForStart();
        chassis.moveWhileMaintainingHeading(-0.1, ChassisController3.whichSide.RIGHT, 4);
        sleep(500);
        telemetry.addData("currentAngle", im.getRelativeAngle());
        telemetry.update();
        while(opModeIsActive()) {
            idle();
        }
    }
}
