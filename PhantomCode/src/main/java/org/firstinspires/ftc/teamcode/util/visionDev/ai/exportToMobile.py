from statistics import mode
import torch
import torchvision
from torch.utils.mobile_optimizer import optimize_for_mobile
import onnx
import onnx2pytorch as o2p

onnx_model = onnx.load(r"/home/raameshb/Documents/GitRepos/PhantomCI/PhantomCode/src/main/java/org/firstinspires/ftc/teamcode/util/vision/ai/yolov5s.onnx")
model = o2p.ConvertModel(onnx_model)

example = model.eval()
print(example)
input()
traced_script_module = torch.jit.trace(model, example)
traced_script_module_optimized = optimize_for_mobile(traced_script_module)
traced_script_module_optimized._save_for_lite_interpreter("./yolov5PytorchMobile.ptl")

example = torch.rand(1, 640, 640, 3)

print (example)