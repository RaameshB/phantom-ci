package org.firstinspires.ftc.teamcode.tests;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.util.RobotConfig;
import org.firstinspires.ftc.teamcode.util.RobotControlClasses.CVC3;
import org.firstinspires.ftc.teamcode.util.enums.LeftAndRightEnum;

//@Disabled
@TeleOp
public class cvTestR extends LinearOpMode {
    RobotConfig robot;
    CVC3 cvWebcam;

    @Override

    public void runOpMode() throws InterruptedException {
        robot = new RobotConfig();
        robot.webcamOnly(hardwareMap, this);
        cvWebcam = new CVC3(robot, this);
        cvWebcam.startAsyncStream();
        while (cvWebcam.getPos() == LeftAndRightEnum.STILL_PROC && !isStopRequested()) {
            idle();
        }
//        cvWebcam.calibrate();
        telemetry.addLine("Waiting For Start...");
        telemetry.update();
        waitForStart();
        while(opModeIsActive()) {
            telemetry.addData("Postion: ", cvWebcam.getPos());
            telemetry.addData("left: ", cvWebcam.getLeftValue());
            telemetry.addData("right: ", cvWebcam.getRightValue());
            telemetry.update();
            idle();
        }
    }
}
